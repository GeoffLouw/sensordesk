import { DataProvider } from './data-provider';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { CanActivate, Router } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router/src';
import { AuthProvider } from './auth-provider';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class AccessProvider implements CanActivate {
  constructor(private authProvider: AuthProvider, private router: Router, public afAuth: AngularFireAuth) {
    // extend
  }
  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean {
    return Observable.create((observer) => {
        console.log('started and Auth checking');
        this.authProvider.authObserver.subscribe((authResult) => {
            let user = this.authProvider.getUser();
            let flag: boolean = true;
            let userDetail = user.userDetail;

            console.log('started and Auth checking');
            console.log('----', userDetail.role);

            if (!userDetail.role) {
                flag = false;
                this.router.navigate(['/login']);
            } else if (state.url.toString().indexOf('/companies') >= 0 && userDetail.role > 100) { // if user is not super admin and section is companies
                flag = false;
                this.router.navigate(['/no-access']);
            } else if (state.url.toString().indexOf('/roles') >= 0 && userDetail.role > 100) { // if user is not super admin and section is roles
                flag = false;
                this.router.navigate(['/no-access']);
            } else if (state.url.toString().indexOf('/users') >= 0 && userDetail.role > 200) { // if user is not super admin and section is roles
                flag = false;
                this.router.navigate(['/no-access']);
            }else if (userDetail) {
                // none
            }
            observer.next(flag);
        });
    });
  }
}
