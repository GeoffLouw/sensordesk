import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RolesProvider {
  private roles = [
    {
      name: 'Super Admin',
      desciption: 'Can edit anything, across all companies.',
      id: 100
    },
    {
      name: 'Admin',
      desciption: 'Can edit mostly anything, across all companies.',
      id: 101
    },
    {
      name: 'Company Admin',
      desciption: 'Can edit anything, across all companies.',
      id: 200
    },
    {
      name: 'Company Installer',
      description: 'Access to companies tasks and assign',
      id: 300
    },
    {
      name: 'Company User',
      description: 'Can access company info.',
      id: 400
    },
    {
      name: 'Customer',
      description: 'No access.',
      id: 500
    }
  ];
  constructor() {
    // extend
  }

  public getRoles() {
    return this.roles;
  }

  public getRole(id) {
    return this.roles[id];
  }
}
