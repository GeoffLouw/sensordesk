import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';

declare let google;
declare let navigator;

const APP_ERRORS = {
  'errors.stored.nodata': 'No data stored, sync your device to pull the latest items.',
  'errors.stored.nouser': 'No user data stored'
};
const GEOLOCATION_ERRORS = {
  'errors.location.unsupportedBrowser': 'Browser does not support location services',
  'errors.location.permissionDenied': 'You have rejected access to your location',
  'errors.location.positionUnavailable': 'Unable to determine your location, please try again.',
  'errors.location.timeout': 'Unable to determine your location, please try again.'
};

@Injectable()
export class LocationProvider {
  constructor (private http: Http) {
    console.log('Hello Locations Provider');
  }

  public getPlaces(address): Observable<any> {
    return Observable.create(observer => {
      let geocoder = new google.maps.Geocoder();
      geocoder.geocode({'address': address }, function (results, status) {
        console.log("ADDRESS:", results);
          if (status === google.maps.GeocoderStatus.OK) {
            observer.next(results[0]);
            observer.complete();
          }else{
            observer.error(results);
          }
      });
    });
  }

  public getLocationByAddress(address): Observable<any> {
    return Observable.create(observer => {
      let geocoder = new google.maps.Geocoder();
      geocoder.geocode({'address': address }, function (results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            console.log("ADDRESS:", results);
            observer.next(results[0]);
            observer.complete();
          }else{
            observer.error(results);
          }
      });
    });
  }

  public getAddressComponents (items: any, val: string): Array<any> {
    let arr = new Array();
    if(items){
      //console.log("---- ITEMS ----", items.length);
      for(let i = 0; i < items.length; i++){
        let itm = items[i];
        if(itm.address && itm.address.address_components){
          for(let a = 0; a < itm.address.address_components.length; a++){
            let component = itm.address.address_components[a];
            
            let hasVal = (component.types.filter((type) => {
              return (type == val);
            }).length > 0);

            if(hasVal){
              let alreadyAdded = (arr.filter((itm) => {
                return itm === component.long_name;
              }).length > 0);
              if(!alreadyAdded) arr.push(component.long_name);
            }
          }
        }
      }
    }
    return arr;
  }

  public getLocationValue (itm, val) {
    let obj = {};
    if(itm.address && itm.address.address_components){
        obj = itm.address.address_components.filter((component) => {
          let hasVal = (component.types.filter((type) => {
            return (type == val);
          }).length > 0);
          return hasVal;
        })[0];
    }
    return obj;
  }

  public getLocationByLatLng(latlng): Observable<any> {
    return Observable.create(observer => {
      let geocoder = new google.maps.Geocoder();
      geocoder.geocode({'location': latlng }, function (results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            console.log("ADDRESS:", results);
            observer.next(results[0]);
            observer.complete();
          }else{
            observer.error(results);
          }
      });
    });
  }

  public getLocationByComponent(addressComponent): Observable<any> {
    return Observable.create(observer => {
      if(addressComponent && addressComponent.geometry && addressComponent.geometry.location){
        observer.next(addressComponent.geometry.location);
        observer.complete();
      }else{
        console.log("No address component");
        // observer.error("no address component");
        // TODO: fix
      }
    });
  }

  public getLocation(showLoading:Boolean = false): Observable<any> {
        return Observable.create(observer => {
        if (window.navigator && window.navigator.geolocation) {
          navigator.geolocation.getCurrentPosition((position) => {
              observer.next(position);
              observer.complete();
            }, (error) => {
              switch (error.code) {
                case 1:
                  observer.error(GEOLOCATION_ERRORS['errors.location.permissionDenied']);
                  break;
                case 2:
                  observer.error(GEOLOCATION_ERRORS['errors.location.positionUnavailable']);
                  break;
                case 3:
                  observer.error(GEOLOCATION_ERRORS['errors.location.timeout']);
                  break;
              }
            });
        }else {
          observer.error(GEOLOCATION_ERRORS['errors.location.unsupportedBrowser']);
        }
		});
    }
}
