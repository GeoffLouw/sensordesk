import { Pipe } from '@angular/core';
import { Injectable } from '@angular/core';

@Pipe({
    name:'filterpipe'
})
@Injectable()

export class FilterPipe{
    public transform(items: any[], args1: any, args2: Number) {
        return items.filter((hero) => {
            return (hero[args1] === args2);
        });
    }
}
