import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
// Do not import from 'firebase' as you'll lose the tree shaking benefits
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';
import { MdDialog, MdDialogRef } from '@angular/material';
import { LocalStorage, SessionStorage, Webstorable } from 'ngx-store';
import { Http, Headers } from '@angular/http';

interface UserObject {
    userFB: any,
    userDetail: any,
    companyID: string
}
type WebstorableObject = Webstorable & UserObject; // save() method is declared in the Webstorable interface
type WebstorableArray = Webstorable & Array<any>;

@Injectable()
export class AuthProvider {
  public authStateFB: Observable<firebase.User>;
  public authState: Subject<any>;
  public currentUser: firebase.User;
  public authObserver: ReplaySubject<any> = new ReplaySubject();
  public apiBase = 'https://firebase.dev.sensornetworks.co.za';

  public userData: WebstorableObject = {
    userFB: {},
    userDetail: {},
    companyID: '',
    save: null
  };
  public roles: any;

  constructor(
    public http: Http, public afAuth: AngularFireAuth, private fbDb: AngularFireDatabase, 
    public dialog: MdDialog) {
    console.log("was auth state created?");
    this.authState = Observable.create((observer) => {
      // this.authObserver = observer;
      console.log("auth state created");
      Subject.create(this.authObserver, observer);
    });
    this.authStateFB = afAuth.authState;
    this.authStateFB.subscribe((user: firebase.User) => {
      // user changed
      this.userData.userFB = user;
      console.log("got firebase auth");
      this.getSignInUser().subscribe((signinResult) => {
        // signinResult
        console.log("got signed in user");
      });
    });

    
  }

  get authenticated(): boolean {
    return this.currentUser !== null;
  }

  public signIn(email, password): Observable<any> {
    let trg = this;
    return Observable.create((observer) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, password).then((response) => {
        trg.userData.userFB = response;
        trg.getSignInUser().subscribe((signinResult) => {
          observer.next(trg.userData);
          observer.complete();
        }, (error) => {
          observer(error);
        });
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  public getSignInUser() {
    let trg = this;
    return Observable.create((observer) => {
      let key = (this.userData.userFB) ? this.userData.userFB.uid : '';
      if (key === '' || key === '0') {
        observer.complete('no key');
        trg.authObserver.next("");
      } else {
        trg.getUserFromDB(key).subscribe((userResult) => {

          trg.setCompany(userResult.companyId); // set company to your current company, extend to save this to dropdown select
          trg.userData.userDetail = userResult;
          /*
          trg.getRoles().subscribe((rolesResult) => {
            trg.roles = rolesResult;
            trg.userData.userDetail.role = trg.getRole(trg.userData.userDetail.roleId);
            trg.authObserver.next(trg.userData);
            observer.next(trg.userData);
            observer.complete();
          });
          */
          trg.userData.userDetail.role = Number(trg.userData.userDetail.roleId);
          trg.authObserver.next(trg.userData);
          observer.next(trg.userData);
          observer.complete();
          
        });
        
      }
    });
  }

  public getUser() {
    return this.userData;
  }
  createAuthorizationHeader(headers: Headers) {
    let tk = this.userData.userFB.Yd;//.getToken().toString();
    //console.log('token', tk, this.userData.userFB);
    headers.append('Authorization', tk);
  }
  getUserFromDB (id: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/users/:id
        let api = this.apiBase + '/api/users/' + id;
        this.http.get(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          observer.next(result);
        });
    });
  }

  /*
  getRoles () {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/roles
        // ?status={{status}}&sort={{sort}}&dir={{dir}}&skip={{skip}}&limit={{limit}}
        let api = this.apiBase + '/api/roles';
        this.http.get(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          observer.next(result);
        });
    });
  }

  getRole (id) {
    let role = this.roles.filter((role) => (role.id == id))[0];
    let code = (role.code) ? role.code : 500;
    return code;
  }
  
  setRole () {

  }
  */

  public watchAuthState() {
    let trg = this;
    return Observable.create((observer) => {
      let key = this.userData.userFB.uid;
      if (key === '' || key === '0') {
        observer.complete('no key');
      } else {
        
        trg.getUserFromDB(key).subscribe((userResult) => {

          trg.userData.userDetail = userResult;
          /*
          trg.getRoles().subscribe((rolesResult) => {
            trg.roles = rolesResult;
            trg.userData.userDetail.role = trg.getRole(trg.userData.userDetail.roleId);
            trg.authObserver.next(trg.userData);
            observer.next(trg.userData);
            observer.complete();
          });
          */
          trg.userData.userDetail.role = Number(trg.userData.userDetail.roleId);
          trg.authObserver.next(trg.userData);
          observer.next(trg.userData);
          observer.complete();
        });
        
        /*
        trg.fbDb.object('/installations/users/' + key).subscribe((userResult) => {
          trg.userData.userDetail = userResult;
          trg.authObserver.next(trg.userData);
          observer.next(trg.userData);
          observer.complete();
        });
        */
      }
    });
  }

  public signOut(): void {
    this.authObserver.next('');
    this.afAuth.auth.signOut();
  }

  public registerUser(user): Observable<any> {
    return Observable.create((observer) => {
      let trg = this;
      this.afAuth.auth.createUserWithEmailAndPassword(user.username, user.password).then((userResponse) => {
        let userObj = {
          name: user.name,
          uid: userResponse.uid,
          email: userResponse.email,
          company: 0
        };
        trg.userData.userFB = userResponse;
        trg.fbDb.object('/installations/users/' + userResponse.uid).set(userObj).then((result) => {
          trg.userData.userDetail = userObj;
          /*
          trg.getRoles().subscribe((rolesResult) => {
            trg.roles = rolesResult;
            trg.userData.userDetail.role = trg.getRole(trg.userData.userDetail.roleId);
            trg.authObserver.next(trg.userData);
            observer.next(userResponse);
            observer.complete();
          });
          */
          trg.userData.userDetail.role = Number(trg.userData.userDetail.roleId);
          trg.authObserver.next(trg.userData);
          observer.next(trg.userData);
          observer.complete();
          
        }).catch((error) => {
          observer.error(error);
        });
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  public setCompany(companyid){
    this.userData.companyID = companyid;
    return Observable.create((observer) => {
      if(companyid !== 0){
        this.userData.companyID = companyid;
        //this.userData.save();
      }else{
        this.userData.companyID = companyid;
      }
      observer.next("");
      observer.complete();
    });
  }

  public getCompany(){
    return this.userData.companyID;
  }

  public createUser(user: any, key: string) {
    return Observable.create((observer) => {
      let trg = this;
      this.afAuth.auth.createUserWithEmailAndPassword(user.email, 'user.password').then((userResponse) => {
        if (key === '' || key === '0') {
          this.fbDb.list('/installations/users').push(user).then((result) => {
            observer.next(result);
          }).catch((error) => {
            observer.error(error);
          });
        } else {
          this.fbDb.object('/installations/users/' + key).set(user).then((result) => {
            observer.next(result);
          }).catch((error) => {
            observer.error(error);
          });
        }
        observer.next(userResponse);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  public displayName(): string {
    if (this.currentUser !== null) {
      return ''; // this.currentUser.facebook.displayName;
    } else {
      return '';
    }
  }

  
}
