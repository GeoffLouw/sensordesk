import { FbProvider } from './fb-provider';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AuthProvider } from './auth-provider';
import { RolesProvider } from './roles-provider';

@Injectable()
export class DataProvider extends FbProvider{ 
  /* Tasks */
  getTasks () {
    return Observable.create((observer) => {
        this.getFbList('tasks').subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  getTask (key) {
    return Observable.create((observer) => {
        this.getFbListItem('tasks', key).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  addTask (obj) {
    return Observable.create((observer) => {
        this.addFbListItem('tasks', obj).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  updateTask (obj: any, key: string) {
    return Observable.create((observer) => {
        this.updateFbListItem('tasks', obj, key).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  removeTask (item: any) {
    let key = item.id;
    return Observable.create((observer) => {
        this.removeFbListByKey('tasks', key).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }

  /* Users */
  getUsers () {
    return Observable.create((observer) => {
        this.getFbList('users').subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  getUser (key: string) {
    return Observable.create((observer) => {
        this.getFbListItem('users', key).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  addUser (obj: any) {
    return Observable.create((observer) => {
        this.addFbListItem('users', obj).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  updateUser (obj: any, key: string) {
    return Observable.create((observer) => {
        this.updateFbListItem('users', obj, key).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  removeUser (item: any) {
    let key = item.id;
    return Observable.create((observer) => {
        this.removeFbListByKey('users', key).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }

  /* Companies */
  getCompanies () {
    return Observable.create((observer) => {
        this.getFbList('companies').subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  getCompany (key: string) {
    return Observable.create((observer) => {
        this.getFbListItem('companies', key).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  addCompany (obj: any) {
    return Observable.create((observer) => {
        this.addFbListItem('companies', obj).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  updateCompany (obj: any, key: string) {
    return Observable.create((observer) => {
        this.updateFbListItem('companies', obj, key).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  removeCompany (item: any) {
    let key = item.id;
    return Observable.create((observer) => {
        this.removeFbListByKey('companies', key).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }

  /* Roles */
  getRoles () {
    return Observable.create((observer) => {
        this.getFbList('roles').subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  getRole (key: string) {
    return Observable.create((observer) => {
        this.getFbListItem('roles', key).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  addRole (obj: any) {
    return Observable.create((observer) => {
        this.addFbListItem('roles', obj).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  updateRole (obj: any, key: string) {
    return Observable.create((observer) => {
        this.updateFbListItem('roles', obj, key).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
  removeRole (item: any) {
    let key = item.id;
    return Observable.create((observer) => {
        this.removeFbListByKey('roles', key).subscribe((result) => {
          observer.next(result);
        }, (error) => {
          observer.error(error);
        });
    });
  }
}