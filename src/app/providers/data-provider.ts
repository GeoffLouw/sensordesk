import { FbProvider } from './fb-provider';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AuthProvider } from './auth-provider';
import { RolesProvider } from './roles-provider';
@Injectable()
export class DataProvider  extends FbProvider{
  public apiBase = 'https://firebase.dev.sensornetworks.co.za';

  public addNote(obj, note) {
    return Observable.create((observer) => {
        if(!obj.notes) obj.notes = new Array();
        let add = {
          timestamp: new Date().getTime().toString(),
          note: note
        }
        obj.notes.push(add);
        observer.next(add);
        observer.complete();
    });
  }

  createAuthorizationHeader(headers: Headers) {
    let tk = this.authProvider.userData.userFB.Yd;//.getToken().toString();
    console.log('token', tk, this.authProvider.userData.userFB);
    headers.append('Authorization', tk);
  }
  
  /* Tasks */
  getTasksAll () {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/tasks
        // ?status={{status}}&sort={{sort}}&dir={{dir}}&skip={{skip}}&limit={{limit}}
        let api = this.apiBase + '/api/tasks';
        this.http.get(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Tasks Results: ', result);
          observer.next(result);
        });
    });
  }
  getTasks () {
    let companyid = this.authProvider.getCompany();
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/companies/:id/tasks
        // ?status={{status}}&sort={{sort}}&dir={{dir}}&skip={{skip}}&limit={{limit}}
        let api = this.apiBase + '/api/companies/' + companyid + '/tasks';
        this.http.get(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Company Tasks Results: ', result);
          observer.next(result);
        });
    });
  }
  getTask (id) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/tasks/:id
        let api = this.apiBase + '/api/tasks/' + id;
        this.http.get(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Task Results: ', result);
          observer.next(result);
        });
    });
  }
  addTask (obj) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/tasks (POST)
        let api = this.apiBase + '/api/tasks';
        this.http.post(api, obj, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Add task result: ', result);
          observer.next(result);
        });
    });
  }
  updateTask (obj: any, id: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/tasks/:id (PUT)
        let api = this.apiBase + '/api/tasks/' + id;
        this.http.put(api, obj, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Update task result: ', result);
          observer.next(result);
        });
    });
  }
  removeTask (obj: any) {
    let id = obj.id;
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // api/tasks/:id (DELETE)
        let api = this.apiBase + '/api/tasks/' + id;
        this.http.delete(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Update task result: ', result);
          observer.next(result);
        });
    });
  }
  /* Users */
  getUsersAll () {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/users
        // ?status={{status}}&sort={{sort}}&dir={{dir}}&skip={{skip}}&limit={{limit}}
        let api = this.apiBase + '/api/users';
        this.http.get(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Users Results: ', result);
          observer.next(result);
        });
    });
  }
  getUsers () {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    let companyid = this.authProvider.getCompany();
    return Observable.create((observer) => {
        // /api/companies/:id/users
        // ?status={{status}}&sort={{sort}}&dir={{dir}}&skip={{skip}}&limit={{limit}}
        let api = this.apiBase + '/api/companies/' + companyid + '/users';
        this.http.get(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Company Users Results: ', result);
          observer.next(result);
        });
    });
  }
  getUser (id: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/users/:id
        let api = this.apiBase + '/api/users/' + id;
        this.http.get(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Users Results: ', result);
          observer.next(result);
        });
    });
  }
  addUser (obj: any) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/users (POST)
        let api = this.apiBase + '/api/users';
        this.http.post(api, obj, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Add user result: ', result);
          observer.next(result);
        });
    });
  }
  updateUser (obj: any, id: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/users/:id (PUT)
        let api = this.apiBase + '/api/users/' + id;
        this.http.put(api, obj, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Update user result: ', result);
          observer.next(result);
        });
    });
  }
  removeUser (obj: any) {
    let id = obj.id;
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // api/tasks/:id (DELETE)
        let api = this.apiBase + '/api/users/' + id;
        this.http.delete(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Update task result: ', result);
          observer.next(result);
        });
    });
  }

  /* Companies */
  getCompanies () {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/companies
        // ?status={{status}}&sort={{sort}}&dir={{dir}}&skip={{skip}}&limit={{limit}}
        let api = this.apiBase + '/api/companies';
        this.http.get(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Companies Results: ', result);
          observer.next(result);
        });
    });
  }
  getCompany (id: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/companies
        // ?status={{status}}&sort={{sort}}&dir={{dir}}&skip={{skip}}&limit={{limit}}
        let api = this.apiBase + '/api/companies/' + id;
        this.http.get(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Companies Results: ', result);
          observer.next(result);
        });
    });
  }
  addCompany (obj: any) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/tasks (POST)
        let api = this.apiBase + '/api/companies';
        this.http.post(api, obj, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Add Company Results: ', result);
          observer.next(result);
        });
    });
  }
  updateCompany (obj: any, id: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/companies/:id (PUT)
        let api = this.apiBase + '/api/companies/' + id;
        this.http.put(api, obj, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Update companies result: ', result);
          observer.next(result);
        });
    });
  }
  removeCompany (obj: any) {
    let id = obj.id;
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
      // /api/companies/:id (PUT)
      let api = this.apiBase + '/api/companies/' + id;
      this.http.delete(api, {
          headers: headers
      }).map((response: any) => response.json()).subscribe((result) => {
        console.log('Deleted companies result: ', result);
        observer.next(result);
      });
    });
  }

  /* Roles */
  getRoles () {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/roles
        // ?status={{status}}&sort={{sort}}&dir={{dir}}&skip={{skip}}&limit={{limit}}
        let api = this.apiBase + '/api/roles';
        this.http.get(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Roles Results: ', result);
          observer.next(result);
        });
    });
  }
  getRole (id: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/roles
        // ?status={{status}}&sort={{sort}}&dir={{dir}}&skip={{skip}}&limit={{limit}}
        let api = this.apiBase + '/api/roles/' + id;
        this.http.get(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Role Results: ', result);
          observer.next(result);
        });
    });
  }
  addRole (obj: any) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/role
        // ?status={{status}}&sort={{sort}}&dir={{dir}}&skip={{skip}}&limit={{limit}}
        let api = this.apiBase + '/api/roles';
        this.http.post(api, obj, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Add Role Results: ', result);
          observer.next(result);
        });
    });
  }
  updateRole (obj: any, id: string) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/role
        // ?status={{status}}&sort={{sort}}&dir={{dir}}&skip={{skip}}&limit={{limit}}
        let api = this.apiBase + '/api/roles';
        this.http.put(api, obj, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Update Role Results: ', result);
          observer.next(result);
        });
    });
  }
  removeRole (obj: any) {
    let id = obj.id;
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return Observable.create((observer) => {
        // /api/role
        let api = this.apiBase + '/api/roles' + id;
        this.http.delete(api, {
          headers: headers
        }).map((response: any) => response.json()).subscribe((result) => {
          console.log('Update Role Results: ', result);
          observer.next(result);
        });
    });
  }
}
