import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AuthProvider } from './auth-provider';
import { RolesProvider } from './roles-provider';


@Injectable()
export class FbProvider {
    constructor (public http: Http, private fbDb: AngularFireDatabase, public authProvider: AuthProvider, private rolesProvider: RolesProvider) {
        // init
    }

  public addFbListItem (type: string, obj: any, key: string = '0') {
    let db = this.fbDb;
    return Observable.create((observer) => {
        if (key === '' || key === '0') {
          this.fbDb.list('/installations/' + type).push(obj).then((result) => {
            observer.next(result);
          }).catch((error) => {
            observer.error(error);
          });
        } else {
          this.fbDb.object('/installations/' + type + '/' + key).set(obj).then((result) => {
            observer.next(result);
          }).catch((error) => {
            observer.error(error);
          });
        }
    });
  }

  public updateFbListItem (type: string, obj: any, key: string) {
    let db = this.fbDb;
    return Observable.create((observer) => {
        this.fbDb.object('/installations/' + type + '/' + key).set(obj).then((result) => {
          observer.next(result);
        }).catch((error) => {
          observer.error(error);
        });
    });
  }

  public removeFbListItem (type: string, item) {
    return Observable.create((observer) => {
          this.fbDb.list('/installations/' + type + '/').remove(item).then((result) => {
            observer.next(result);
          });
      });
  }

  public removeFbListByKey (type: string, key: string) {
    return Observable.create((observer) => {
          this.fbDb.list('/installations/' + type + '/' + key).remove().then((result) => {
            observer.next(result);
          });
      });
  }

  public getFbListItem (type: string, key: string) {
    return Observable.create((observer) => {
          if (key === '' || key === '0') {
            observer.complete('no key');
          } else {
            this.fbDb.object('/installations/' + type + '/' + key).subscribe((result) => {
              observer.next(result);
            });
          }
      });
  }

  public getFbList (type: string, filter: any = null) {
    return Observable.create((observer) => {
        let user = this.authProvider.getUser();
        if ((type === 'tasks' || type === 'users' || type === 'customers')) {
          let companyid = this.authProvider.getCompany();
          filter = {
            orderByChild: 'company',
            equalTo: companyid
          };
        }
        let query = (filter) ? filter : {};
        console.log('Query is: ', query);
        this.fbDb.list('/installations/' + type + '/', { query }).subscribe((result) => {
          console.log('List Result:', result);
          observer.next(result);
        });
      });
  }
}