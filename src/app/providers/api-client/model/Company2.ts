/**
 * Sensor Networks Installer API
 * API for the geyser installer web and mobile apps
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface Company2 {
    tags?: Array<string>;

    id?: string;

    name: string;

    detail?: string;

    email: string;

    tel: string;

    address?: models.CompaniesAddress;

    emails?: Array<string>;

    tels?: Array<string>;

    website?: string;

    socialMedia?: Array<models.CompaniesSocialMedia>;

}
