import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TypeProvider {
  private types = [
    {
      name: 'Appointment',
      id: 100
    },
    {
      name: 'Task',
      id: 200
    }
  ];

  constructor() {
    // extend
  }

  public getTypes() {
    return this.types;
  }
  public getType(id) {
    return this.types[id];
  }
}
