import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class StatusProvider {
  private statuses = [
    {
      name: 'New',
      id: 100,
      color: 'primary'
    },
    {
      name: 'Open',
      id: 200,
      color: 'primary'
    },
    {
      name: 'Pending',
      id: 300,
      color: 'accent'
    },
    {
      name: 'Cancelled',
      id: 300,
      color: 'accent'
    },
    {
      name: 'Closed',
      id: 400,
      color: ''
    }
  ];

  public getStatuses() {
    return this.statuses;
  }
  public getStatus(id) {
    return this.statuses[id];
  }
}
