import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { NoContentComponent } from './no-content';
import { DashboardComponent } from './dashboard';
import { LoginComponent } from './login';
import { SettingsComponent } from './settings';
import { DataResolver } from './app.resolver';
import { RegisterUserComponent } from './register-user';
import { AccessProvider } from './providers/access-provider';
import { NoAccessComponent } from './no-access/no-access.component';

export const ROUTES: Routes = [
  { path: '',      component: DashboardComponent, canActivate: [AccessProvider] },
  { path: 'home',  component: DashboardComponent, canActivate: [AccessProvider] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AccessProvider] },
  { path: 'settings', component: SettingsComponent, canActivate: [AccessProvider] },
  { path: 'settings', component: SettingsComponent, canActivate: [AccessProvider] },

  { path: 'tasks', loadChildren: './tasks#TasksModule', canActivate: [AccessProvider] },
  { path: 'tasks-install', loadChildren: './tasks#TasksModule', canActivate: [AccessProvider] },
  { path: 'appointments', loadChildren: './appointments#AppointmentsModule', canActivate: [AccessProvider] },
  { path: 'roles', loadChildren: './roles#RolesModule', canActivate: [AccessProvider] },
  { path: 'companies', loadChildren: './companies#CompaniesModule', canActivate: [AccessProvider] },
  { path: 'customers', loadChildren: './customers#CustomersModule', canActivate: [AccessProvider] },
  { path: 'users', loadChildren: './users#UsersModule', canActivate: [AccessProvider] },
  
  
  { path: 'no-access', component: NoAccessComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterUserComponent },
  { path: '**',    component: NoContentComponent }
];
