import { TaskNotesModule } from './../task-notes';
import { CalendarModule, CheckboxModule } from 'primeng/primeng';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './appointments.routes';
import { AppointmentsComponent } from './appointments.component';
import { AppMaterialModule } from '../app.material.module';

@NgModule({
  declarations: [
    AppointmentsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    AppMaterialModule,
    FlexLayoutModule,
    CalendarModule,
    CheckboxModule,
    TaskNotesModule
  ],
})
export class AppointmentsModule {
  public static routes = routes;
}
