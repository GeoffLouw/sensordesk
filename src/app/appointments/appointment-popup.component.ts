import { Router } from '@angular/router';
import { LocationProvider } from './../providers/location-provider';
import { AuthProvider } from './../providers/auth-provider';
import { DataProvider } from './../providers/data-provider';
import { Component, ElementRef, ViewChild, Renderer2  } from '@angular/core';
import { MdDialogRef, MdDialog, MdDialogConfig, MdSidenav, MdListModule } from '@angular/material';
import { ConfirmDialogComponent } from './../_components/confirm-dialog.component';


declare let google;

interface customerObject {
  name: string,
  email: string,
  address: string,
  tasks: Array<any>,
  id: string
}

@Component({
  selector: 'page-appointments.component',
  template: `<md-sidenav-container class="example-sidenav-fab-container">
        <md-sidenav #customerSidenav mode="over" opened="false">
            <div class="example-scrolling-content">
                <div *ngIf="selCustomer" class="customer-detail">
                    <h4>Customer Info:</h4>
                    <strong>{{selCustomer.name}}</strong><br>
                    {{selCustomer.email}}<br>
                    {{selCustomer.address}}<br>
                    <div class="appointment-buttons">
                        <button md-raised-button color="primary" (click)="createAppointment()">Make Appointment</button>
                    </div>
                </div>
            </div>
        </md-sidenav>
        <md-tab-group>
            <md-tab label="Map">
                <div #singlemap id="singlemap" class="singlemap"></div>
            </md-tab>
            <md-tab label="List">
                <div  id="customerList" #customerList>
                    <md-nav-list *ngIf="customers">
                      <md-list-item *ngFor="let customer of customers">
                        <div (click)="selectCustomer(customer)">
                            {{ customer.name }}
                        </div>
                      </md-list-item>
                    </md-nav-list>
                </div>
            </md-tab>
        </md-tab-group>
    </md-sidenav-container>`
})

export class AppointmentsPopupComponent {
  @ViewChild('singlemap') public mapElement: ElementRef;
  @ViewChild('customerSidenav') public sideNavElement: MdSidenav;
  public user: any;
  public map: any;
  public icons: any = {
    blue: {
      icon: 'assets/img/mapicons/pin_blue.png'
    },
    red: {
      icon: 'assets/img/mapicons/pin_red.png'
    },
    yellow: {
      icon: 'assets/img/mapicons/pin_yellow.png'
    }
  };

  public customers: customerObject;
  public selCustomer: any;
  public tasks: any;


  constructor(
    private dataProvider: DataProvider,
    private dialog: MdDialog,
    public authProvider: AuthProvider,
    public renderer: Renderer2,
    public locationProvider: LocationProvider,
    public router: Router,
    public dialogRef: MdDialogRef<AppointmentsPopupComponent>
    ) {
    this.user = this.authProvider.getUser();
      
    /* Customers */
    this.dataProvider.getTasks().subscribe((tasksResults) => {
      this.tasks = tasksResults;
    });
    // console.log('MAP: ', this.mapElement, document.getElementById('singlemap'));
    // this.loadMap();
  }
  ngAfterViewInit() {
      // onsole.log('on after view init', this.mapElement);
      // this returns null
      this.loadMap();
  }

  public renderOnMap(customers: any){
    let bounds = new google.maps.LatLngBounds();
    let trg = this;
    for(let i=0; i < customers.length; i++){
      let customer = customers[i];
      customer.tasks = this.tasks.filter((task) => (task.customer == customer.id));
      let iconimg = (customer.tasks && customer.tasks.length > 0) ? 'yellow' : 'blue';
      console.log(customer.tasks);
      this.locationProvider.getLocationByComponent(customer.address).subscribe((result) => {
          let position = result;
          let marker = new google.maps.Marker({
            map: this.map,
            position: position,
            icon: this.icons[iconimg].icon, //defined by status (belongs to any appointments)
          });
          marker.setPosition(position);
          bounds.extend(position);
          this.map.fitBounds(bounds);
          google.maps.event.addListener(marker, 'click', () => {
            this.selectCustomer(customer);
          });
      })
    }
  }

  public selectCustomer(customer){
    this.sideNavElement.open().then((result) => {
        console.log('why is this sometimes lagging?');
    });
    this.selCustomer = customer;
  }
  
  public createAppointment(){
    this.dialogRef.close(this.selCustomer);
  }
  public loadMap () {
    let latLng = new google.maps.LatLng(-33.9224853,18.4072265);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: true,
      rotateControl: true,
      fullscreenControl: true
    };
    
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let myloc = new google.maps.Marker({
        clickable: false,
        icon: new google.maps.MarkerImage('assets/img/mapicons/mobile_marker.png',
                                                        new google.maps.Size(22, 22),
                                                        new google.maps.Point(0, 0),
                                                        new google.maps.Point(11, 11),
                                                        new google.maps.Size(22, 22)
                                                        ),
        shadow: null,
        zIndex: 999,
        map: this.map
    });

    let circle = new google.maps.Circle({
      strokeColor: '#2f73c2',
      strokeOpacity: 0.0,
      strokeWeight: 0,
      map: this.map,
      radius: 100,    // 10 miles in metres
      fillColor: '#2f73c2'
    });

    circle.bindTo('center', myloc, 'position');
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      google.maps.event.trigger(this.map, 'resize');
    });

    this.locationProvider.getLocation().subscribe(
      (position) => {
        let mylocation = position;
        let me = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        myloc.setPosition(me);
        /* Dont extend bounds for current location
        bounds.extend(me);
        this.map.fitBounds(bounds);
        */
    });
  }
  
}
