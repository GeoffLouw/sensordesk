import { TaskNotes } from './../task-notes/task-notes.component';
import { Router } from '@angular/router';
import { LocationProvider } from './../providers/location-provider';
import { AuthProvider } from './../providers/auth-provider';
import { DataProvider } from './../providers/data-provider';
import { Component, ElementRef, ViewChild, Renderer2, ChangeDetectorRef } from '@angular/core';
import { MdDialogRef, MdDialog, MdDialogConfig, MdSidenav } from '@angular/material';
import { ConfirmDialogComponent } from './../_components/confirm-dialog.component';
import { AppointmentDetailComponent } from "./appointment-detail/appointment-detail.component";
import { AppointmentsPopupComponent } from "./appointment-popup.component";

declare let google;
declare let MarkerClusterer;

@Component({
  selector: 'page-appointments.component',
  templateUrl: 'appointments.component.html',
  styleUrls: ['appointments.component.scss']
})

export class AppointmentsComponent {
  @ViewChild('singlemap') public mapElement: ElementRef;
  @ViewChild('customerSidenav') public sideNavElement: MdSidenav;
  public items: any;
  public user: any;
  public map: any;
  public icons: any = {
    blue: {
      icon: 'assets/img/mapicons/pin_blue.png'
    },
    red: {
      icon: 'assets/img/mapicons/pin_red.png'
    },
    yellow: {
      icon: 'assets/img/mapicons/pin_yellow.png'
    }
  };
  public customers: any;
  public users: any;
  public selCustomer: any;
  public searchText: string;
  public markers: Array<any> = new Array();
  public itemsResult: any;
  public markerCluster;
  public clusterStyles = [
    {
      textColor: 'white',
      url: 'assets/img/mapicons/m1.png',
      height: 53,
      width: 52
    },
    {
      textColor: 'white',
      url: 'assets/img/mapicons/m2.png',
      height: 56,
      width: 55
    },
    {
      textColor: 'white',
      url: 'assets/img/mapicons/m3.png',
      height: 66,
      width: 65
    }
  ];
  public clusteroptions = {
      imagePath: 'assets/img/mapicons/m',
      styles: this.clusterStyles
  };
  public filterObj = {
    name: '',
    area: '',
    agent: '',
    status: 'new,cancelled',
    dates: {
      start: null,
      end: null
    }
  }
  public counter = {
    total: 0,
    count: 0
  }
  public areas: Array<string>;



  constructor(
    private dataProvider: DataProvider,
    private dialog: MdDialog,
    public authProvider: AuthProvider,
    public renderer: Renderer2,
    public locationProvider: LocationProvider,
    public router: Router,
    private cd: ChangeDetectorRef
    ) {
    
    
  }
  init() {
    this.clearMap();
    this.user = this.authProvider.getUser();
    console.log("I ask for it:");
    this.dataProvider.getTasks().subscribe((result) => {
      console.log("I got it:", result);
      this.itemsResult = result;
      this.filterTasks();
      
      this.dataProvider.getUsers().subscribe((usersResults) => {
        this.users = usersResults;
      });
      
      this.getAreas('locality');
    });
    this.loadMap();
  }
  ngAfterViewInit() {
      this.init();
  }
  public getAreas (area) {
    let arr = this.locationProvider.getAddressComponents(this.itemsResult, area);
    this.areas = arr;
  }
  
  public createAppointment(){
    this.router.navigate(['./appointments/appointment-detail'],{queryParams: {id:this.selCustomer.id,customer:this.selCustomer.id}});
  }
  
  public launchCustomers(item) {
    let dialogRef = this.dialog.open(AppointmentsPopupComponent, {
      height: '600px',
      width: '800px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.selCustomer = result;
        this.createAppointment();
      }
    });
  }

  public renderOnMap(customers: any){
    let bounds = new google.maps.LatLngBounds();
    let trg = this;
    this.markers = new Array();
    for(let i=0; i < customers.length; i++){
      let customer = customers[i];
      /*customer.tasks = this.items.filter((task) => (task.customer == customer.id));
      let iconimg = (customer.tasks && customer.tasks.length > 0) ? 'yellow' : 'blue';*/
      let marker = new google.maps.Marker({
        map: this.map,
        icon: this.icons['blue'].icon, //defined by status (belongs to any appointments)
      });
      this.locationProvider.getLocationByComponent(customer.address).subscribe((result) => {
          let position = result;
          marker.setPosition(position);
          bounds.extend(position);
          this.map.fitBounds(bounds);
          google.maps.event.addListener(marker, 'click', () => {
            console.log("click!");
            this.selectTask(customer);
          });
      });
      this.markers.push(marker);
    }

    this.markerCluster = new MarkerClusterer(this.map, this.markers, this.clusteroptions);
    this.markerCluster.setMaxZoom(16);
  }

  public getAddressLines(address){
    return address.split(',');
  }
  
  public clearDates() {
    this.filterObj.dates.start = null;
    this.filterObj.dates.end = null;
    this.filterTasks();
  }

  public filterTasks(){
    this.items = [];
    
    // filter on status
    this.items = this.itemsResult.filter((item) => {
      let flag = false;
      let statuses = this.filterObj.status.split(",");
      for(let i = 0; i < statuses.length; i++){
        if(statuses[i] == item.status){
          flag = true;
          break;
        }
      }
      return flag;
    });
    this.counter.total = this.items.length;
    // filter by owner name
    this.items = this.items.filter((item) => (item.ownerName.toLowerCase().indexOf(this.filterObj.name.toLowerCase()) >= 0));
    // filter by agent
    this.items = this.items.filter((item) => {
      let flag = (item.userId.indexOf(this.filterObj.agent) >= 0);
      console.log(flag, item.userId, this.filterObj.agent)
      return flag;
    });
    
    // filter by area
    if(this.filterObj.area.length > 0){
      this.items = this.items.filter((item) => {
        let flag = false;
        let loc: any = this.locationProvider.getLocationValue(item, 'locality');
        if(loc.long_name == this.filterObj.area) flag = true;
        return flag;
      });
    }

    if(this.filterObj.dates.start && this.filterObj.dates.end){
      this.items = this.items.filter((item) => {
        let flag = false;
        let d1 = new Date(this.filterObj.dates.start);
        d1.setHours(0);
        d1.setMinutes(0);
        d1.setSeconds(0);
        let d2 = new Date(this.filterObj.dates.end);
        d2.setHours(23);
        d2.setMinutes(59);
        d2.setSeconds(59);
        let d3 = new Date(item.assignedDate);

        if(d3 > d1 && d3 < d2){
          flag = true;
        }
        return flag;
      });
    }
    
    this.counter.count = this.items.length;
    this.clearMap();
    this.renderOnMap(this.items);
  }
  public clearMap (){
    console.log(this.markers);
    for(var i=0; i<this.markers.length; i++){
      let marker = this.markers[i];
      marker.setMap(null);
      console.log(marker);
    }
    this.markers = [];
    if(this.markerCluster)  this.markerCluster.setMap(null);
  }

  public selectTask(customer){
    this.selCustomer = customer;
    this.sideNavElement.open().then((result) => {
        console.log('why is this sometimes lagging?');
    });
    this.cd.detectChanges();

  }
  
  public loadMap () {
    let latLng = new google.maps.LatLng(-33.9224853,18.4072265);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: true,
      rotateControl: true,
      fullscreenControl: true
    };
    
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let myloc = new google.maps.Marker({
        clickable: false,
        icon: new google.maps.MarkerImage('assets/img/mapicons/mobile_marker.png',
                                                        new google.maps.Size(22, 22),
                                                        new google.maps.Point(0, 0),
                                                        new google.maps.Point(11, 11),
                                                        new google.maps.Size(22, 22)
                                                        ),
        shadow: null,
        zIndex: 999,
        map: this.map
    });

    let circle = new google.maps.Circle({
      strokeColor: '#2f73c2',
      strokeOpacity: 0.0,
      strokeWeight: 0,
      map: this.map,
      radius: 100,    // 10 miles in metres
      fillColor: '#2f73c2'
    });

    circle.bindTo('center', myloc, 'position');
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      google.maps.event.trigger(this.map, 'resize');
    });

    this.locationProvider.getLocation().subscribe(
      (position) => {
        let mylocation = position;
        let me = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        myloc.setPosition(me);
    });
  }
  public deleteItem(item) {
    let dialogRef: MdDialogRef<ConfirmDialogComponent>;
    dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.componentInstance.title = 'Confirmation';
    dialogRef.componentInstance.message = 'Are you sure?';
    dialogRef.afterClosed().subscribe((confirmResponse) => {
      if (confirmResponse) {
        this.clearMap();
        this.dataProvider.removeTask(item).subscribe((removeResponse) => {
          console.log(removeResponse);
          setTimeout(() => {
            this.init();
          },2000);
        });
      }
    });
  }
}
