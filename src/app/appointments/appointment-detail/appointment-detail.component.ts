import { Task1 } from './../../providers/api-client/model/Task1';
import { AlertDialogComponent } from './../../_components/alert-dialog.component';
import { MdDialogRef, MdDialog } from '@angular/material';
import { StatusProvider } from './../../providers/status-provider';
import { TypeProvider } from './../../providers/types-provider';
import { DataProvider } from '../../providers/data-provider';
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthProvider } from '../../providers/auth-provider';
import { DateTimePickerModule } from 'ngx-datetime-picker';
import { Location } from '@angular/common';


declare let google;
interface userObject {
  name: string,
  detail: string,
  assignedDate: number,
  userId: string,
  type: any,
  status: any,
  company: string,
  date: Date
}
@Component({
  selector: 'appointment-detail',
  templateUrl: 'appointment-detail.component.html',
  styleUrls: ['./appointment-detail.component.scss']
})
export class AppointmentDetailComponent implements OnInit {
  @ViewChild('singlemap') public mapElement: ElementRef;
  public key: string = '';
  public item: Task1 = {
    ownerName: '',
    detail: '',
    userId: '',
    companyId: '',
    assignedDate: '',
    status: Task1.StatusEnum.New,
    type: Task1.TypeEnum.Installation
  };

  
  public types: any;
  public statuses: any;
  public companies: any;
  public agents: any;
  public agent: any;
  public customers: any;
  public user: any;
  public install: any;
  public customer: Object;

  public lat: number = 51.678418;
  public lng: number = 7.809007;
  public map: any;
  public assignedDate: Date = new Date();
  public agentTasks;
  public tasks: any;
  public dateTimeExample = new Date();

  public events:Array<any> = [];
  public event: MyEvent;
  public tempevent: MyEvent;
  public dialogVisible: boolean = false;
  public idGen: number = 100;
  public eventDate: Date;
  public header: any;

  constructor(
    private route: ActivatedRoute,
    private dataProvider: DataProvider,
    private authProvider: AuthProvider,
    private router: Router,
    private typeProvider: TypeProvider,
    private statusProvider: StatusProvider,
    private cd: ChangeDetectorRef,
    private dialog: MdDialog,
    private location: Location
    ) {
    // init
  }
  public ngOnInit() {
    // console.log('hello `ChildDetail` component');
    // this.items = db.list('/items');
    this.user = this.authProvider.getUser();
    this.key = this.route.snapshot.queryParams['id'];
    if(this.key === '0'){
      this.item.companyId = this.authProvider.getCompany();

      this.getCustomerAndAgent();
      this.types = this.typeProvider.getTypes();
      this.statuses = this.statusProvider.getStatuses();
    }
    
    this.header = {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		};

    let trg = this;
    // this.loadMap();
    this.dataProvider.getTasks().subscribe((taskResults) => {
      this.tasks = taskResults;
      if(this.key !== '0'){
        this.dataProvider.getTask(this.key).subscribe((result) => {
          trg.item = result;
          trg.assignedDate = new Date(trg.item.assignedDate);
          
          console.log(trg.item.assignedDate);
          if(result.userId) trg.getAgentDetail(result.userId);

          this.types = this.typeProvider.getTypes();
          this.statuses = this.statusProvider.getStatuses();
          this.getCustomerAndAgent();
        });
      }
    });
  }

  public onCalendarInit(event){
    console.log("INITIALISED!", event);
  }
  public handleDayClick(e){
    let evtDate = new Date(e.date.format());
    evtDate.setHours(9);
    let nowDate = new Date();
    console.log(evtDate);
    console.log("---", nowDate);
    console.log("===", evtDate < nowDate);
    if(evtDate < nowDate){
      let message = 'Can\'t schedule an event in the past, please select a later date.';
      this.doError(message);
    }else if(this.agent){
      if(!this.tempevent){
        this.tempevent = new MyEvent();
      }else{
        this.deleteEvent();
        this.tempevent = new MyEvent();
      }
      /*this.event.id = this.key;*/
      //this.eventDate = new Date(e.date.format());
      this.tempevent.title = this.item.ownerName;
      this.tempevent.start = evtDate;
      this.tempevent.task = this.item;
      this.dialogVisible = true;
      this.cd.detectChanges();
    }else{
      let message = 'Select an agent to schedule a task.';
      this.doError(message);
    }
  }
  public doError (message) {
      let dialogRef: MdDialogRef<AlertDialogComponent>;
      dialogRef = this.dialog.open(AlertDialogComponent);
      dialogRef.componentInstance.title = 'Error';
      dialogRef.componentInstance.message = message;
      return dialogRef.afterClosed();
  }
  public handleEventClick(e){
    let thisevent = e.calEvent;
    let thistask = thisevent.task;
    let msg = '<p class="label-sub">' + thisevent.start.format('MMMM Do YYYY, h:mm:ss a') + '<p>';
    msg += '<p><strong>Contact number</strong><br>' + thistask.tel+ '</p>';
    msg += '<p><strong>Email</strong><br>' + thistask.email+ '</p>';
    msg += '<p><strong>Address</strong><br>';
    console.log(thistask);

    if(thistask.address.formatted_address){
      let addressarray = thistask.address.formatted_address.split(',');
      for(let i = 0; i < addressarray.length; i++){
        msg += addressarray[i] + '<br>';
      }
    }

    msg += '</p>';

    let dialogRef: MdDialogRef<AlertDialogComponent>;
    dialogRef = this.dialog.open(AlertDialogComponent);
    dialogRef.componentInstance.title = thistask.ownerName;
    dialogRef.componentInstance.message = msg;
    return dialogRef.afterClosed();
  }
  public saveEvent() {
      //update
      if(this.tempevent && this.tempevent.id) {
          //this.deleteEvent();
      } else {
          if(this.validateTime(this.tempevent.start)){
            this.tempevent.id = this.idGen++;
            let idx = this.events.indexOf(this.event);
            if(idx >= 0){
              this.events.splice(idx,1);
            }
            this.event = JSON.parse(JSON.stringify(this.tempevent));
            this.events.push(this.event);
            this.tempevent = null;
          }
          
      }
      
      this.dialogVisible = false;
      this.cd.detectChanges();
  }

  public validateTime(dt) {
    let flag = true;

    let sdate = new Date(dt);
    let now = new Date();
    now.setHours(now.getHours()+2);

    

    for(let i = 0; i < this.events.length; i++) {
      let thisevent = this.events[i];
      let thisstart = new Date(thisevent.start);
      let hoursoffset = this.hoursOffset(thisstart, dt);
      if(hoursoffset < 2){
        flag = false;
        let message = 'Appointment time is too close to another.';
        this.doError(message);
        break;
      }
    }
    console.log(sdate.getTime() - now.getTime());
    if(sdate.getTime() - now.getTime() < 0){
      flag = false;
      let message = 'Appointment time is too soon. Please select a later time.';
      this.doError(message);
    }
    

    return flag;
  }
  public hoursOffset(d1, d2) {

    return Math.abs(d1.getTime() - d2.getTime()) / 36e5;
  }
  
  public deleteEvent() {
      /*
      let index: number = this.findEventIndexById(this.event.id);
      console.log(index);
      if(index > 0) {
          this.events.splice(index, 1);
      }
      this.dialogVisible = false;
      this.event = null;
      */
  }
  public eventRenderer(event, element){
    console.log("this is: ", event, element);
    for(var i=0; i < element.length; i++){
      var el = element[i];
       el.innerHTML = event.start.format('LTS');
    }
  }
  public findEventIndexById(id: number) {
      let index = -1;
      for(let i = 0; i < this.events.length; i++) {
          if(id == this.events[i].id) {
              index = i;
              break;
          }
      }
      
      return index;
  }
  public getAddressLines(address){
    return address.split(',');
  }
  public getAgentDetail(agentId: string){
    console.log("AGENT: ", agentId, this.tasks);
    this.agentTasks = this.tasks.filter((item) => (item.userId && (item.userId.indexOf(agentId) !== -1)));
    this.events = new Array();
    
    for(var i=0; i<this.agentTasks.length; i++){
        let event = {
          title: this.agentTasks[i].name,
          start: this.agentTasks[i].assignedDate,
          task: this.agentTasks[i]
        }
        if(this.agentTasks[i].assignedDate){
          if(this.agentTasks[i].id !== this.key){
            //this 
            this.events.push(event);
          }else{
            this.event = new MyEvent();
          }
        }
    }
    
    if(agentId && agentId.length > 0){
      this.dataProvider.getUser(agentId).subscribe((agentResult) => {
        this.agent = agentResult;
      });
    }
  }

  public getCustomerAndAgent() {
    this.dataProvider.getUsers().subscribe((usersResult) => {
        this.agents = usersResult.filter((hero) => {
          return true;//(hero.role <= 400);
        });
        if(this.item.userId) this.getAgentDetail(this.item.userId);
      });
  }
  public saveTask(){
    if(this.event && this.event.start){
      this.item.assignedDate = this.event.start.toString();
    }
    this.item.type = Task1.TypeEnum.Installation;
    this.item.status = Task1.StatusEnum.New;
    if(this.key === '0'){
      this.dataProvider.addTask(this.item).subscribe((result) => {
        this.location.back();
      });
    }else{
      this.dataProvider.updateTask(this.item, this.key).subscribe((result) => {
        this.location.back();
      });
    }
  }
  public saveAsTask(){
    this.item.assignedDate = this.event.start.toString();
    this.item.type = Task1.TypeEnum.Installation;
    this.item.status = Task1.StatusEnum.Open;
    /*
    this.item.notes.push({
      timestamp: new Date().getTime().toString(),
      note: "Updated: test2"
    });
    */
    if(this.key === '0'){
      this.dataProvider.addTask(this.item).subscribe((result) => {
        this.router.navigate(['../../appointments']);
      });
    }else{
      this.dataProvider.updateTask(this.item, this.key).subscribe((result) => {
        this.router.navigate(['../../appointments']);
      });
    }
  }
  public editTask() {
    this.router.navigate(['/tasks/task'],{queryParams: {id:this.key}});
  }
  public onSubmit() {
    this.item.assignedDate = this.event.start.toString();
    /*
    this.item.notes.push({
      timestamp: new Date().getTime(),
      note: "Updated: here"
    });
    */
    if(this.key === '0'){
      this.dataProvider.addTask(this.item).subscribe((result) => {
        console.log('Result is: ', result);
        setTimeout(() => {
          this.router.navigate(['../../appointments']);
        },2000);
      });
    }else{
      this.dataProvider.updateTask(this.item, this.key).subscribe((result) => {
        console.log('Result is: ', result);
        setTimeout(() => {
          this.router.navigate(['../../appointments']);
        },2000);
      });
    }
  }
  public cancel() {
    this.location.back();
  }
}



export class MyEvent {
    id: number;
    title: string = "New Event";
    start: Date;
    end: string;
    allDay: boolean = false;
    task: any;
}
