import { AppointmentDetailComponent } from './appointment-detail.component';

export const routes = [
  { path: '', component: AppointmentDetailComponent,  pathMatch: 'full' },
];
