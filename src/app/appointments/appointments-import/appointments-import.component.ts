import { LocationProvider } from './../../providers/location-provider';
import { Observable } from 'rxjs/Rx';
import { Task1 } from './../../providers/api-client/model/Task1';
import { AlertDialogComponent } from './../../_components/alert-dialog.component';
import { MdDialogRef, MdDialog } from '@angular/material';
import { StatusProvider } from './../../providers/status-provider';
import { TypeProvider } from './../../providers/types-provider';
import { DataProvider } from '../../providers/data-provider';
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthProvider } from '../../providers/auth-provider';
import { DateTimePickerModule } from 'ngx-datetime-picker';
import { Location } from '@angular/common';
import * as Papa from 'papaparse';


declare let google;
@Component({
  selector: 'appointments-import',
  templateUrl: 'appointments-import.component.html',
  styleUrls: ['./appointments-import.component.scss']
})
export class AppointmentsImportComponent implements OnInit {

  public csvData: Array<any>;
  public fields: any = ["None", "Name", "Email", "Phone","Address"];
  public fieldsIdx: any = [];
  public includeArr: Array<boolean> = new Array();
  public step: number = 1;
  public inprogress: boolean = false;
  public errorCells: Array<any> = new Array();
  public successCells: Array<any> = new Array();
  public tasks: Array<any> = new Array();
  public errorMsg = "";


  constructor(
    private route: ActivatedRoute,
    private dataProvider: DataProvider,
    private authProvider: AuthProvider,
    private router: Router,
    private typeProvider: TypeProvider,
    private statusProvider: StatusProvider,
    private cd: ChangeDetectorRef,
    private dialog: MdDialog,
    private location: Location,
    private locationProvider: LocationProvider
    ) {
    // init
  }
  public ngOnInit() {
    // angular init
    
  }

  public changeListener (evt) {
    this.readThis(evt.target);
    console.log("changed!");
  }

  public readThis (inputVal: any) {
    let self = this;
    let file: File = inputVal.files[0];
    Papa.parse(file, {
      header: false,
      dynamicTyping: false,
      complete: function(results) {
        let data = [];
        for(let i = 0; i < results.data.length; i++){
          self.includeArr.push(true);
        }
        console.log(results.data);
        self.csvData = results.data;
        self.doStep(2);
      }
    });
    /*
    let myReader: FileReader = new FileReader();
    myReader.onloadend = function(e) {
        console.log(myReader.result);
        self.parseCSV(myReader.result);
    }
    myReader.readAsText(file);
    */
  }

  public getField (idxstr) {
    console.log(idxstr, this.fields);
    var idx = Number(this.fieldsIdx[idxstr]);
    return this.fields[idxstr];
  }

  public validateData () {
    this.inprogress = true;
    this.errorMsg = "";

    // check all fields
    let fieldFlag = true;
    let f1 = this.fieldsIdx.indexOf("Name");
    let f2 = this.fieldsIdx.indexOf("Email");
    let f3 = this.fieldsIdx.indexOf("Phone");
    let f4 = this.fieldsIdx.indexOf("Address");
    
    if(f1 < 0 || f2 < 0 || f3 < 0 || f4 <0){
      fieldFlag = false;
    }
    console.log(f1, f2, f3, f4, fieldFlag);
    // done checking fields

    // validate fields
    let dataFlag = true;
    if(fieldFlag){
      this.errorCells = new Array();
      this.successCells = new Array();
      this.tasks = new Array();
      for(let i = 0; i < this.csvData.length; i++){
        // loop over rows
        let row = this.csvData[i];
        if(this.includeArr[i]){
          let nameVal = this.nameValidation(row[f1]);
          let emailVal = this.emailValidation(row[f2]);
          let numberVal = this.numberValidation(row[f3]);
          let addressVal = this.nameValidation(row[f4]);
          let task = {
            userId: '',
            companyId: this.authProvider.getCompany(),
            ownerName: row[f1],
            status: Task1.StatusEnum.New,
            addressFld: row[f4],
            email: row[f2],
            tel: row[f3],
            type: Task1.TypeEnum.Installation,
            rowid: i
          }
          console.log(task);
          this.tasks.push(task);
          this.errorCells[i] = [];
          this.successCells[i] = [];
          if(!nameVal){
            this.errorCells[i][f1] = true;
            dataFlag = false;
          }
          if(!emailVal){
            this.errorCells[i][f2] = true;
            dataFlag = false;
          } 
          if(!numberVal){
            this.errorCells[i][f3] = true;
            dataFlag = false;
          }
          if(!addressVal){
            this.errorCells[i][f4] = true;
            dataFlag = false;
          }
          
        }else{
          console.log("Dont check: ", i);
        }
        
      }
    }
    
    if(!fieldFlag){
      this.errorMsg = 'Missing fields, assign all fields.';
    }else if(!dataFlag){
      this.errorMsg = 'Invalid data, see the highlighted fields below.';
    }
    
    if(dataFlag && fieldFlag){
      this.doStep(3);
      this.inprogress = false;
    }else{
      this.inprogress = false;
    }
  }

  public hasError (idx, adx) {
    let flag = false;
    let rows = this.errorCells[idx];
    if(rows && rows[adx]) flag = true;
    return flag;
  }

  public hasSuccess (idx, adx) {
    let flag = false;
    let rows = this.successCells[idx];
    if(rows && rows[adx]) flag = true;
    return flag;
  }

  public nameValidation (val) {
    return (val.toString().length > 1);
  }
  public numberValidation (val) {
    var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
    let flag = val.match(phoneno);
    return flag
  }
  public emailValidation (val) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(val);
  }

  public validateAddress (val) {
    this.inprogress = true;
    let addr = "";
    let self = this;
    this.checkAddress(0).subscribe((result) => {
      let f4 = this.fieldsIdx.indexOf("Address");
      this.successCells[this.tasks[result.id].rowid][f4] = true;
      this.tasks[result.id].address = result.data;
      this.cd.detectChanges();
      console.log(this.successCells[this.tasks[result.id].rowid], this.successCells[this.tasks[result.id].rowid][f4]);
    }, (error) => {

    }, () => {
      
      this.inprogress = false;
      this.doStep(4);
      this.cd.detectChanges();
    });
  }

  public checkAddress (rowid, obs = null) {
    let observer;
    let self = this;
    console.log("once per row", rowid);
    setTimeout(() => {
        let row = this.tasks[rowid];
        let addr = row.addressFld;
        console.log(rowid, row, addr);
        this.locationProvider.getLocationByAddress(addr).subscribe((addrResult) => {
          observer.next({
            data: addrResult,
            id: rowid
          });
          //console.log("Result", addrResult);
          if(rowid < this.tasks.length-1){
            let nextid = rowid+1;
            self.checkAddress(nextid, observer);//'result.upload.token');
          }else{
            observer.complete();
          }
        }, (error) => {
          observer.error(error);
          if(rowid < this.tasks.length-1){
            let nextid = rowid+1;
            self.checkAddress(nextid, observer);//'result.upload.token');
          }else{
            observer.complete();
          }
        }, () => {

        });
    },100);

    let obsobj;
    if(obs){
        observer = obs;
    }else{
        obsobj = Observable.create(obs => {
            observer = obs;
        });
    }
    return obsobj;
  }

  public syncTasks (val) {
    this.inprogress = true;
    let addr = "";
    let self = this;
    this.syncTask(0).subscribe((result) => {
      this.successCells[this.tasks[result.id].rowid][0] = true;
      this.tasks[result.id].address = result.data;
      this.cd.detectChanges();
    }, (error) => {

    }, () => {
      this.inprogress = false;
      this.doStep(5);
      this.cd.detectChanges();
    });
  }

  public syncTask (rowid, obs = null) {
    let observer;
    let self = this;
    console.log("once per row", rowid);
    setTimeout(() => {
        let row = this.tasks[rowid];
        let taskobj = JSON.parse(JSON.stringify(this.tasks[rowid]));
        delete taskobj.rowid;
        delete taskobj.addressFld;
        this.dataProvider.addTask(taskobj).subscribe(() => {
          observer.next({
            data: this.tasks[rowid],
            id: rowid
          });
          //console.log("Result", addrResult);
          if(rowid < this.tasks.length-1){
            let nextid = rowid+1;
            self.syncTask(nextid, observer);//'result.upload.token');
          }else{
            observer.complete();
          }
        }, (error) => {

        }, () => {

        });
    },100);

    let obsobj;
    if(obs){
        observer = obs;
    }else{
        obsobj = Observable.create(obs => {
            observer = obs;
        });
    }
    return obsobj;
  }

  public goBack () {
    this.step = this.step-1;
    if(this.step == 0) this.step = 1;
    this.cd.detectChanges();
  }
  public doStep (num) {
    this.step = num;
    this.cd.detectChanges();
  }
  public reset() {
    this.csvData = new Array()
    this.fieldsIdx = new Array();
    this.includeArr = new Array();
    this.step = 1;
    this.inprogress = false;
    this.errorCells = new Array();
    this.successCells = new Array();
    this.tasks = new Array();
    this.errorMsg = "";
  }
  
  public updateView () {
    console.log("update");
    this.cd.detectChanges();
  }
  
}
