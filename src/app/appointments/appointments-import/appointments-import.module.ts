import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './appointments-import.routes';
import { AppointmentsImportComponent } from './appointments-import.component';
import { AppMaterialModule } from '../../app.material.module';
import { DateTimePickerModule } from 'ngx-datetime-picker';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import { ScheduleModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';


@NgModule({
  declarations: [
    AppointmentsImportComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    AppMaterialModule,
    DateTimePickerModule,
    NKDatetimeModule,
    ScheduleModule,
    DialogModule,
    CalendarModule,
    CheckboxModule
  ],
  providers: []
})
export class AppointmentsImportModule {
  public static routes = routes;
}
