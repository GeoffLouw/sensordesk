import { AppointmentsImportComponent } from './appointments-import.component';

export const routes = [
  { path: '', component: AppointmentsImportComponent,  pathMatch: 'full' },
];
