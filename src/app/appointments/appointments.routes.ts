import { AppointmentsComponent } from './appointments.component';

export const routes = [
  { path: '', children: [
    { path: '', component: AppointmentsComponent },
    { path: 'appointment-detail', loadChildren: './appointment-detail#AppointmentDetailModule' },
    { path: 'appointments-import', loadChildren: './appointments-import#AppointmentsImportModule' }
  ]},
];
