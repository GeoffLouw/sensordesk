
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TaskNotes } from './task-notes.component';
import { AppMaterialModule } from '../app.material.module';

@NgModule({
  declarations: [
    TaskNotes
  ],
  imports: [
    CommonModule,
    /* RouterModule.forChild(routes), */
    AppMaterialModule
  ],
  exports: [
      TaskNotes
  ]
})
export class TaskNotesModule {
  
}
