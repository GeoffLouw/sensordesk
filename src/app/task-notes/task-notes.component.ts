import { Router } from '@angular/router';
import { DataProvider } from './../providers/data-provider';
import { Task1 } from './../providers/api-client/model/Task1';
import { NoteDialogComponent } from './../_components/note-dialog.component';
import { BrowserModule } from '@angular/platform-browser';
import { Component, Input, NgModule } from '@angular/core';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { ConfirmDialogComponent } from './../_components/confirm-dialog.component';

@Component({
  selector: 'task-notes',
  templateUrl: 'task-notes.component.html',
  styleUrls: ['./task-notes.component.scss']
})

export class TaskNotes {
  @Input() task: Task1;
  @Input() limitTo: number = 5;

  public hasmore: boolean = false;


  constructor( private dialog: MdDialog, private dataProvider: DataProvider, private router: Router) {
    
  }

  public addNote() {
    if(!this.task.notes) this.task.notes = new Array();
    let dialogRef: MdDialogRef<NoteDialogComponent>;
    dialogRef = this.dialog.open(NoteDialogComponent);
    dialogRef.componentInstance.title = 'Note';
    dialogRef.componentInstance.message = 'Are you sure?';
    dialogRef.afterClosed().subscribe((noteResponse) => {
      if (noteResponse) {
        this.task.notes.push({
          note: noteResponse,
          timestamp: new Date().getTime().toString()
        });
        this.dataProvider.updateTask(this.task, this.task.id).subscribe((result) => {
          console.log("got result");
        });
      }
    });
  }

  public viewAll () {
    let path = '/tasks/task-detail';
    this.router.navigate([path],{queryParams: {id:this.task.id}});
  }
  public hasMore() {
    let flag = (this.task && this.task.notes && this.task.notes.length > this.limitTo);
    console.log(flag);
    return flag;
  }

  public getNotes() {
    let arr: any = [];
    if(this.task && this.task.notes){
      for(let i = this.task.notes.length - this.limitTo; i < this.task.notes.length; i++){
        if(this.task.notes[i]){
          arr.push(this.task.notes[i]);
        }
      }
    }
    return arr;
  }
}
