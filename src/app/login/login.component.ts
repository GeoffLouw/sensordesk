import { AuthProvider } from '../providers/auth-provider';
import { MdDialogModule } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardComponent } from '../dashboard';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { AlertDialogComponent } from '../_components/alert-dialog.component';

@Component({
  selector: 'page-login',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent {
  public user: any = {
    username: '',
    password: ''
  };

  constructor(public auth: AuthProvider, private router: Router, private dialog: MdDialog) {
    // init
  }

  public onSubmit() {
    this.auth.signIn(this.user.username, this.user.password).subscribe((user) => {
      console.log('got it:', user);
      this.router.navigate(['/dashboard']);
    }, (error) => {
      let message = (error.message) ? error.message : 'Failed to login, please check your details and try again.';
      let dialogRef: MdDialogRef<AlertDialogComponent>;
      dialogRef = this.dialog.open(AlertDialogComponent);
      dialogRef.componentInstance.title = 'Error';
      dialogRef.componentInstance.message = message;
      return dialogRef.afterClosed();
    });
  }
  public signOut() {
    this.auth.signOut();
  }
  public forgotPassword() {
    // to extend?
  }
}
