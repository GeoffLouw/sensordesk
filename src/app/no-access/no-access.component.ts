import { Component } from '@angular/core';

@Component({
  selector: 'no-access',
  template: `
    <div>
      <h1>No Access</h1>
      <p>You dont have access, ask an administrator to assign a role to this profile.</p>
    </div>
  `
})
export class NoAccessComponent {

}
