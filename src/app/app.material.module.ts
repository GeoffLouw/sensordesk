import { NgModule, ApplicationRef } from '@angular/core';
import { MdInputModule } from '@angular/material';
import { MdSelectModule } from '@angular/material';
import { MdMenuModule } from '@angular/material';
import { MdSidenavModule } from '@angular/material';
import { MdToolbarModule } from '@angular/material';
import { MdListModule } from '@angular/material';
import { MdCardModule } from '@angular/material';
import { MdButtonModule } from '@angular/material';
import { MdDialogModule } from '@angular/material';
import { MdTooltipModule } from '@angular/material';
import { MdDatepickerModule } from '@angular/material';
import { MdCheckboxModule, MdNativeDateModule } from '@angular/material';
import { MdIconModule } from '@angular/material';
import { MdGridListModule } from '@angular/material';
import { MdChipsModule, MdTabsModule } from '@angular/material';

@NgModule({
  imports: [
    MdGridListModule, MdChipsModule,
    MdNativeDateModule, MdIconModule,
    MdInputModule, MdSelectModule,
    MdMenuModule, MdSidenavModule,
    MdToolbarModule, MdListModule,
    MdCardModule, MdButtonModule,
    MdDialogModule, MdTooltipModule,
    MdDatepickerModule, MdCheckboxModule,
    MdTabsModule
  ],
  exports: [
    MdGridListModule, MdChipsModule,
    MdNativeDateModule, MdIconModule,
    MdInputModule, MdSelectModule,
    MdMenuModule, MdSidenavModule,
    MdToolbarModule, MdListModule,
    MdCardModule, MdButtonModule,
    MdDialogModule, MdTooltipModule,
    MdDatepickerModule, MdCheckboxModule,
    MdTabsModule
  ],
})
export class AppMaterialModule { }
