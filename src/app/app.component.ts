import { DataProvider } from './providers/data-provider';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppState } from './app.service';
import { AuthProvider } from './providers/auth-provider';
import { Router } from '@angular/router';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.scss'
  ],
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  public angularclassLogo = 'assets/img/angularclass-avatar.png';
  public name = 'Sensordesk';
  public url = '';
  public authed = false;
  public user: any;
  public companies: any;
  public company: string;

  constructor(public appState: AppState, public authProvider: AuthProvider, private router: Router, dataProvider: DataProvider) {
    authProvider.authObserver.subscribe((user) => {
      this.user = user;
      this.authed = (user) ? true : false;
      if(this.authed){
        dataProvider.getCompanies().subscribe((companiesResult) => {
          this.companies = companiesResult;
          this.company = this.authProvider.getCompany();
        });
      }
    });
  }
  public setCompany(companyid){
    this.authProvider.setCompany(companyid).subscribe((result) => {
      this.router.navigate(['/dashboard']);
    });
  }
  public signOut() {
    this.authProvider.signOut();
    this.router.navigate(['/login']);
  }

  public ngOnInit() {
    // none
  }

}
