import { MdDialogRef } from '@angular/material';
import { Component } from '@angular/core';

@Component({
    selector: 'note-dialog',
    template: `
        <p>{{ title }}</p>
        <div>
        <md-input-container>
            <textarea mdInput placeholder="add note" [(ngModel)]="note" name="noteFld" id="noteFld" #noteFld="ngModel"></textarea>
        </md-input-container>
        </div>
        <button type="button" md-raised-button 
            (click)="dialogRef.close(note)">OK</button>
        <button type="button" md-button 
            (click)="dialogRef.close()">Cancel</button>
    `,
})
export class NoteDialogComponent {
    public title: string;
    public message: string;
    public note: string;
    constructor(public dialogRef: MdDialogRef<NoteDialogComponent>) {
        // to extend
    }
}
