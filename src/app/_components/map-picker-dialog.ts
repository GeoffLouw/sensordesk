import { MdDialogRef } from '@angular/material';
import { Component } from '@angular/core';

@Component({
    selector: 'alert-dialog',
    template: `
        <p>{{ title }}</p>
        <p>{{ message }}</p>
        <button type="button" md-button 
            (click)="dialogRef.close()">OK</button>
    `,
})
export class AlertDialogComponent {
    public title: string;
    public message: string;
    constructor(public dialogRef: MdDialogRef<AlertDialogComponent>) {
        // to extend
    }
}
