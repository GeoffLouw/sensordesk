import { Observable } from 'rxjs/Rx';
import { AlertDialogComponent } from './alert-dialog.component';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { Injectable } from '@angular/core';

@Injectable()
export class AlertDialogsService {

    constructor(private dialog: MdDialog) { }

    public confirm(title: string, message: string): Observable<boolean> {
        let dialogRef: MdDialogRef<AlertDialogComponent>;
        dialogRef = this.dialog.open(AlertDialogComponent);
        dialogRef.componentInstance.title = title;
        dialogRef.componentInstance.message = message;
        return dialogRef.afterClosed();
    }
}
