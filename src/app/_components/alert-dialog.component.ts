import { MdDialogRef } from '@angular/material';
import { Component } from '@angular/core';

@Component({
    selector: 'alert-dialog',
    template: `<div class='{{cssclass}}'>
        <h3>{{ title }}</h3>
        <p [innerHTML]='message'></p>
        <button type="button" md-button 
            (click)="dialogRef.close()">OK</button>
        </div>
    `,
    styles: ['h3 { margin:0 0 10px 0; font-size: 18px; }','.testclass{ max-width: 250px; min-width:250px; }']
})
export class AlertDialogComponent {
    public title: string;
    public message: string;
    public cssclass: string = 'testclass';
    constructor(public dialogRef: MdDialogRef<AlertDialogComponent>) {
        // to extend
    }
}
