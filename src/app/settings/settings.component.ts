import { Component } from '@angular/core';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent {

  constructor() {
    // empty for now
  }
}
