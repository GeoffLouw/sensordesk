import { DataProvider } from './../providers/data-provider';
import { Component } from '@angular/core';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { ConfirmDialogComponent } from './../_components/confirm-dialog.component';

@Component({
  selector: 'page-roles',
  templateUrl: 'roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent {
  public items: any;
  public rolesItems: any;
  constructor(
    private dataProvider: DataProvider,
    private dialog: MdDialog
    ) {
      this.dataProvider.getRoles().subscribe((listResult) => {
      this.items = listResult;
    });
  }

  public deleteItem(item) {
    let dialogRef: MdDialogRef<ConfirmDialogComponent>;
    dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.componentInstance.title = 'Confirmation';
    dialogRef.componentInstance.message = 'Are you sure?';
    dialogRef.afterClosed().subscribe((confirmResponse) => {
      if (confirmResponse) {
        this.dataProvider.removeRole(item).subscribe((rolesResponse) => {
          console.log(rolesResponse);
        });
      }
    });
  }
}
