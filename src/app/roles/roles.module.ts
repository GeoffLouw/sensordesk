import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './roles.routes';
import { RolesComponent } from './roles.component';
import { AppMaterialModule } from '../app.material.module';

@NgModule({
  declarations: [
    RolesComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    AppMaterialModule,
    FlexLayoutModule
  ],
})
export class RolesModule {
  public static routes = routes;
}
