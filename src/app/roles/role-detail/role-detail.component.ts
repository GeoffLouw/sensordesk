import { RolesProvider } from './../../providers/roles-provider';
import { DataProvider } from '../../providers/data-provider';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'role-detail',
  templateUrl: 'role-detail.html',
  styleUrls: ['./role-detail.scss']
})
export class RoleDetailComponent implements OnInit {
  public key: string = '';
  public item: any = {
    role: '',
    detail: '',
  };
  private roles: any;
  constructor(
    public db: AngularFireDatabase,
    private route: ActivatedRoute,
    private dataProvider: DataProvider,
    private rolesProvider: RolesProvider,
    private router: Router
    ) {
    // this.items = db.list('/items');
    this.key = this.route.snapshot.queryParams['id'];
    let trg = this;

    if(this.key !== '0'){
      this.dataProvider.getRole(this.key).subscribe((result) => {
        trg.item = result;
      }, (error) => {
          console.log(error);
      });
    }
    this.roles = this.rolesProvider.getRoles();
  }

  public ngOnInit() {
    // console.log('hello `ChildDetail` component');
  }

  public onSubmit() {
    if(this.key !== '0'){
      this.dataProvider.updateRole(this.item, this.key).subscribe((result) => {
        console.log('Result is: ', result);
        this.router.navigate(['../../roles']);
      });
    }else{
      this.dataProvider.addRole(this.item).subscribe((result) => {
        console.log('Result is: ', result);
        this.router.navigate(['../../roles']);
      });
    }
  }
}
