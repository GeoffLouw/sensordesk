import { RoleDetailComponent } from './role-detail.component';

export const routes = [
  { path: '', component: RoleDetailComponent,  pathMatch: 'full' },
];
