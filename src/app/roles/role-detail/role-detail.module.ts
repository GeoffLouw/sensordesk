import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './role-detail.routes';
import { RoleDetailComponent } from './role-detail.component';
import { AppMaterialModule } from '../../app.material.module';

@NgModule({
  declarations: [
    // Components / Directives/ Pipes
    RoleDetailComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    // materialdesign
    AppMaterialModule
  ],
  providers: []
})
export class RoleDetailModule {
  public static routes = routes;
}
