import { RolesComponent } from './roles.component';

export const routes = [
  { path: '', children: [
    { path: '', component: RolesComponent },
    { path: 'role-detail', loadChildren: './role-detail#RoleDetailModule' }
  ]},
];
