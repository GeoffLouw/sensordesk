import { DataProvider } from './../providers/data-provider';
import { AuthProvider } from '../providers/auth-provider';
import { MdDialogModule } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardComponent } from '../dashboard';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { AlertDialogComponent } from '../_components/alert-dialog.component';

@Component({
  selector: 'page-register',
  templateUrl: 'register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})

export class RegisterUserComponent {
  public user = {
    username: '',
    password: '',
    name: ''
  };
  constructor(
    public auth: AuthProvider,
    private router: Router,
    private dialog: MdDialog,
    private dataProvider: DataProvider
    ) {

  }

  public onSubmit() {
    this.auth.registerUser(this.user).subscribe(
      (userResponse) => {
        this.router.navigate(['/dashboard']);
      }, (error) => {
        let message = (error.message) ? error.message : 'Failed to login, please check your details and try again.';
        let dialogRef: MdDialogRef<AlertDialogComponent>;
        dialogRef = this.dialog.open(AlertDialogComponent);
        dialogRef.componentInstance.title = 'Error';
        dialogRef.componentInstance.message = message;
        return dialogRef.afterClosed();
      }
    );
  }
}
