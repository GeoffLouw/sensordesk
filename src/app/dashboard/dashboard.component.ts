import { Component, OnInit, ViewChild, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataProvider } from "../providers/data-provider";
import { AuthProvider } from "../providers/auth-provider";
import { MdDialogRef, MdDialog, MdDialogConfig, MdSidenav } from '@angular/material';
import { LocationProvider } from "../providers/location-provider";

declare let google;
declare let MarkerClusterer;

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild('singlemap') public mapElement: ElementRef;
  @ViewChild('customerSidenav') public sideNavElement: MdSidenav;
  public localState: any;
  public items: any;
  public itemsResult: any;
  public map: any;
  public markers: Array<any> = new Array();
  public icons: any = {
    blue: {
      icon: 'assets/img/mapicons/pin_blue.png'
    },
    red: {
      icon: 'assets/img/mapicons/pin_red.png'
    },
    yellow: {
      icon: 'assets/img/mapicons/pin_yellow.png'
    },
    orange: {
      icon: 'assets/img/mapicons/pin_orange.png'
    }
  };
  public tasks: any;
  public selTask: any;
  public selAgent: any;
  public searchText: string;
  public markerCluster;
  public users: any;
  
  public clusterStyles = [
    {
      textColor: 'white',
      url: 'assets/img/mapicons/m1.png',
      height: 53,
      width: 52
    },
    {
      textColor: 'white',
      url: 'assets/img/mapicons/m2.png',
      height: 56,
      width: 55
    },
    {
      textColor: 'white',
      url: 'assets/img/mapicons/m3.png',
      height: 66,
      width: 65
    }
  ];
  public clusteroptions = {
      imagePath: 'assets/img/mapicons/m',
      styles: this.clusterStyles
  };
  public filterObj = {
    name: '',
    area: '',
    agent: '',
    status: ['new','pending'],
    dates: {
      start: null,
      end: null
    }
  }
  public counter = {
    total: 0,
    count: 0
  }
  public areas: Array<string>;

  constructor(
    private dataProvider: DataProvider,
    private authProvider: AuthProvider,
    private locationProvider: LocationProvider,
    private route: ActivatedRoute,
    public router: Router,
    private cd: ChangeDetectorRef
  ) {

  }
  public ngOnInit() {
    this.init();
  }

  public init(){
    this.route
      .data
      .subscribe((data: any) => {
        // your resolved data from route
        this.localState = data.yourData;
      });
    
    
    this.dataProvider.getTasks().subscribe((itemsResult) => {
      this.itemsResult = itemsResult;
      this.getAreas('locality');
      this.filterTasks();
      this.dataProvider.getUsers().subscribe((usersResults) => {
        this.users = usersResults;
      });
    });
    this.loadMap();
  }


  public renderOnMap(tasks: any){
    console.log("rendering on map", tasks);
    let bounds = new google.maps.LatLngBounds();
    let trg = this;
    this.markers = new Array();
    for(let i=0; i < tasks.length; i++){
      let task = tasks[i];
      /*customer.tasks = this.items.filter((task) => (task.customer == customer.id));
      let iconimg = (customer.tasks && customer.tasks.length > 0) ? 'yellow' : 'blue';*/
      let clr = (task.status === 'new' || task.status === 'cancelled' || task.status === 'pending') ? 'blue' : 'orange';
      if(task.status === 'open') clr = 'yellow';
      this.locationProvider.getLocationByComponent(task.address).subscribe((result) => {
          let position = result;
          let marker = new google.maps.Marker({
            map: this.map,
            position: position,
            icon: this.icons[clr].icon, //defined by status (belongs to any appointments)
          });
          marker.setPosition(position);
          bounds.extend(position);
          this.map.fitBounds(bounds);
          google.maps.event.addListener(marker, 'click', () => {
            this.selectTask(task);

            console.log("what happened");
          });

          this.markers.push(marker);
      }, (error) => {
        console.log("error loading address");
      })
    }


    this.markerCluster = new MarkerClusterer(this.map, this.markers, this.clusteroptions);
    this.markerCluster.setMaxZoom(16);
  }

  public getAreas (area) {
    let arr = this.locationProvider.getAddressComponents(this.itemsResult, area);
    this.areas = arr;
  }

  public getAddressLines(address){
    return address.split(',');
  }

  public clearDates() {
    this.filterObj.dates.start = null;
    this.filterObj.dates.end = null;
    this.filterTasks();
  }
  
  public filterTasks(){
    this.items = [];
    
    // filter on status
    // filter by owner name
    this.items = this.itemsResult.filter((item) => (item.ownerName.toLowerCase().indexOf(this.filterObj.name.toLowerCase()) >= 0));
    this.counter.total = this.items.length;
    // filter by agent
    this.items = this.items.filter((item) => (item.userId.indexOf(this.filterObj.agent) >= 0));
    // filter by area
    if(this.filterObj.area.length > 0){
      this.items = this.items.filter((item) => {
        let flag = false;
        let loc: any = this.locationProvider.getLocationValue(item, 'locality');
        if(loc.long_name == this.filterObj.area) flag = true;
        return flag;
      });
    }

    if(this.filterObj.dates.start && this.filterObj.dates.end){
      this.items = this.items.filter((item) => {
        let flag = false;
        let d1 = new Date(this.filterObj.dates.start);
        d1.setHours(0);
        d1.setMinutes(0);
        d1.setSeconds(0);
        let d2 = new Date(this.filterObj.dates.end);
        d2.setHours(23);
        d2.setMinutes(59);
        d2.setSeconds(59);
        let d3 = new Date(item.assignedDate);

        console.log("1", d1);
        console.log("2", d2);
        console.log("3", d3);
        console.log(d1 < d3 && d3 < d2);

        if(d3 > d1 && d3 < d2){
          flag = true;
        }
        return flag;
      });
    }

    this.counter.count = this.items.length;
    this.clearMap();
    this.renderOnMap(this.items);
  }

  public selectTask(task){
    this.sideNavElement.open().then((result) => {
        console.log('why is this sometimes lagging?');
    });
    this.selTask = task;
    this.selAgent = null;
    if(this.selTask.userId && this.selTask.userId.length > 0){
      this.dataProvider.getUser(this.selTask.userId).subscribe((userResult) => {
        this.selAgent = userResult;
      });
    }
    this.cd.detectChanges();
  }
  public onSearchChange(e){
    this.items = this.itemsResult.filter((item) => (item.ownerName.toLowerCase().indexOf(this.searchText.toLowerCase()) >= 0));
    console.log(this.items);
    this.clearMap();
    this.renderOnMap(this.items);
  }
  public clearMap (){
    for(var i=0; i<this.markers.length; i++){
      let marker = this.markers[i];
      marker.setMap(null);
      marker = null;
    }
    this.markers = new Array();
    if(this.markerCluster)  this.markerCluster.setMap(null);
  }
  public loadMap () {
    let latLng = new google.maps.LatLng(-33.9224853,18.4072265);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: true,
      rotateControl: true,
      fullscreenControl: true
    };
    
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let myloc = new google.maps.Marker({
        clickable: false,
        icon: new google.maps.MarkerImage('assets/img/mapicons/mobile_marker.png',
                                                        new google.maps.Size(22, 22),
                                                        new google.maps.Point(0, 0),
                                                        new google.maps.Point(11, 11),
                                                        new google.maps.Size(22, 22)
                                                        ),
        shadow: null,
        zIndex: 999,
        map: this.map
    });

    let circle = new google.maps.Circle({
      strokeColor: '#2f73c2',
      strokeOpacity: 0.0,
      strokeWeight: 0,
      map: this.map,
      radius: 100,    // 10 miles in metres
      fillColor: '#2f73c2'
    });

    circle.bindTo('center', myloc, 'position');
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      google.maps.event.trigger(this.map, 'resize');
    });

    this.locationProvider.getLocation().subscribe(
      (position) => {
        let mylocation = position;
        let me = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        myloc.setPosition(me);
        /* Dont extend bounds for current location
           bounds.extend(me);
           this.map.fitBounds(bounds); */
    });
  }
  
  public viewTask (){
    let path = './tasks/task-detail';
    if(this.selTask.status === 'new' || this.selTask.status === 'pending' || this.selTask.status === 'cancelled') path = './appointments/appointment-detail';
    if(this.selTask.status === 'open') path = './tasks/task-detail';
    if(this.selTask.status === 'closed') path = './tasks/task-detail';
    this.router.navigate([path],{queryParams: {id:this.selTask.id}});
  }
  public openTask(task) {
    this.selTask = task;
    this.viewTask();
  }
}
