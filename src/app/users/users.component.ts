import { AuthProvider } from './../providers/auth-provider';
import { RolesProvider } from './../providers/roles-provider';
import { DataProvider } from './../providers/data-provider';
import { Component } from '@angular/core';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { ConfirmDialogComponent } from './../_components/confirm-dialog.component';

@Component({
  selector: 'page-users',
  templateUrl: 'users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent {
  public items: any;
  public roles: any;
  public user: any;

  constructor(
    private dataProvider: DataProvider,
    private dialog: MdDialog,
    public rolesProvider: RolesProvider,
    public authProvider: AuthProvider
    ) {
    this.roles = this.rolesProvider.getRoles();
    this.user = this.authProvider.getUser();
    this.dataProvider.getUsers().subscribe((result) => {
      this.items = result;
      for (let i = 0; i < this.items.length; i++) {
        let itm = this.items[i];
        itm.company_name = 'none';
        this.dataProvider.getCompany(this.items[i].company).subscribe(
          (companyResult) => {
            itm.company_name = (companyResult) ? companyResult.name : 'none';
          });
        let role = itm.role_name = this.roles.filter((hero) => {
            return (hero.id === itm.role);
          })[0];
        itm.role_name = (role) ? role.name : 'none';
      }
    });
  }
  public deleteItem(item) {
    let dialogRef: MdDialogRef<ConfirmDialogComponent>;
    dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.componentInstance.title = 'Confirmation';
    dialogRef.componentInstance.message = 'Are you sure?';
    dialogRef.afterClosed().subscribe((confirmResponse) => {
    if (confirmResponse) {
        this.dataProvider.removeUser(item).subscribe((removeResponse) => {
          console.log(removeResponse);
        });
      }
    });
  }
}
