import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './users.routes';
import { UsersComponent } from './users.component';
import { AppMaterialModule } from '../app.material.module';

@NgModule({
  declarations: [
    UsersComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    AppMaterialModule,
    FlexLayoutModule
  ],
})
export class UsersModule {
  public static routes = routes;
}
