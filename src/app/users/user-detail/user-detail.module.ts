import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './user-detail.routes';
import { UserDetailComponent } from './user-detail.component';
import { AppMaterialModule } from '../../app.material.module';

@NgModule({
  declarations: [
    // Components / Directives/ Pipes
    UserDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    // materialdesign
    AppMaterialModule
  ],
})
export class UserDetailModule {
  public static routes = routes;
}
