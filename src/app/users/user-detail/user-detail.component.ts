import { DataProvider } from '../../providers/data-provider';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthProvider } from '../../providers/auth-provider';

@Component({
  selector: 'user-detail',
  templateUrl: 'user-detail.html',
  styleUrls: ['./user-detail.scss']
})

export class UserDetailComponent implements OnInit {
  public key: string = '';
  public item: any = {
    firstName: '',
    detail: '',
    companyId: '',
    email: '',
    roleId: 500,
    tel: ''
  };
  public user
  public companies;
  public roles;
  public address_text: string;

  constructor(
    private route: ActivatedRoute,
    private dataProvider: DataProvider,
    private authProvider: AuthProvider,
    private router: Router) {
    // this.items = db.list('/items');
  }

  public ngOnInit() {
    this.key = this.route.snapshot.queryParams['id'];
    let trg = this;
    // console.log('hello `ChildDetail` component');
    
    if (this.key === '0') {
      this.getRoles();
    }else {
      this.dataProvider.getUser(this.key).subscribe((result) => {
        console.log("Getting user: ", this.key);
        trg.item = result;
        this.getRoles();
      }, (error) => {
          // console.log(error);
      });
    }

    this.user = this.authProvider.getUser();
    if (this.key === '0') {
      /*this.item.company = this.user.userDetail.company;*/
      this.item.companyId = this.authProvider.getCompany();
    }
  }

  public getRoles() {
    let trg = this;
    this.dataProvider.getCompanies().subscribe((companiesResult) => {
      this.companies = companiesResult;
    }, (error) => {
        // console.log(error);
    });
    this.dataProvider.getRoles().subscribe((rolesResult) => {
      this.roles = rolesResult;
      console.log('roles loaded', this.roles);
    });
  }

  public roleChange() {
    if (this.item.role < 200) {
      console.log('Super Admin: No company');
      this.item.companyId = 0;
    }
  }

  public onSubmit() {
    if(this.key !== '0'){
      this.dataProvider.updateUser(this.item, this.key).subscribe((result) => {
        this.router.navigate(['../../users']);
      });
    }else{
      this.dataProvider.addUser(this.item).subscribe((result) => {
        this.router.navigate(['../../users']);
      });
    }
  }

  public addUser() {
    if(this.key !== '0'){
      this.dataProvider.updateUser(this.item, this.key).subscribe((result) => {
        this.router.navigate(['../../users']);
      });
    }else{
      this.dataProvider.addUser(this.item).subscribe((result) => {
        this.router.navigate(['../../users']);
      });
    }
  }
}
