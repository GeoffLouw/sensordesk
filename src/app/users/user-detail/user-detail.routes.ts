import { UserDetailComponent } from './user-detail.component';

export const routes = [
  { path: '', component: UserDetailComponent,  pathMatch: 'full' },
];
