import { AuthProvider } from './../providers/auth-provider';
import { Component, OnInit } from '@angular/core';
import { DataProvider } from './../providers/data-provider';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { ConfirmDialogComponent } from './../_components/confirm-dialog.component';

@Component({
  selector: 'page-customers',
  templateUrl: 'customers.component.html',
  styleUrls: ['./customers.component.scss']
})

export class CustomersComponent {
  public items: any;
  public user: any;
  constructor(
    private dataProvider: DataProvider,
    private dialog: MdDialog,
    public authProvider: AuthProvider
    ) {
    this.user = this.authProvider.getUser();
    this.dataProvider.getFbList('customers').subscribe((result) => {
      this.items = result;
      for (let i = 0; i < this.items.length; i++) {
        let itm = this.items[i];
        this.dataProvider.getFbListItem('companies',
          this.items[i].company).subscribe((companyResult) => {
            itm.company_name = companyResult.name;
          });
      }
    });
  }

  public deleteItem(item) {
    let dialogRef: MdDialogRef<ConfirmDialogComponent>;
    dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.componentInstance.title = 'Confirmation';
    dialogRef.componentInstance.message = 'Are you sure?';
    dialogRef.afterClosed().subscribe((confirmResponse) => {
    if (confirmResponse) {
        this.dataProvider.removeFbListItem('customers', item).subscribe((removeResponse) => {
          console.log(removeResponse);
        });
      }
    });
  }
}
