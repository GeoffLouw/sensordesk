import { CustomersComponent } from './customers.component';

export const routes = [
  { path: '', children: [
    { path: '', component: CustomersComponent },
    { path: 'customer-detail', loadChildren: './customer-detail#CustomerDetailModule' }
  ]},
];
