import { CustomerDetailComponent } from './customer-detail.component';

export const routes = [
  { path: '', component: CustomerDetailComponent,  pathMatch: 'full' },
];
