import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './customer-detail.routes';
import { CustomerDetailComponent } from './customer-detail.component';
import { AppMaterialModule } from '../../app.material.module';

@NgModule({
  declarations: [
    // Components / Directives/ Pipes
    CustomerDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    // materialdesign
    AppMaterialModule
  ],
})
export class CustomerDetailModule {
  public static routes = routes;
}
