import { DataProvider } from '../../providers/data-provider';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthProvider } from '../../providers/auth-provider';

@Component({
  selector: 'customer-detail',
  templateUrl: 'customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})

export class CustomerDetailComponent implements OnInit {
  public key: string = '';
  public item: any = {
    name: '',
    detail: '',
    company: '',
    email: '',
    role: 500
  };
  public companies;
  public roles;
  public user;

  constructor(
    private route: ActivatedRoute,
    private dataProvider: DataProvider,
    private authProvider: AuthProvider,
    private router: Router) {
    // this.items = db.list('/items');
  }
  public ngOnInit() {
    this.key = this.route.snapshot.queryParams['id'];
    let trg = this;
    if (this.key === '0') {
      this.item.company = this.authProvider.getCompany();
    }else {
      this.dataProvider.getFbListItem('customers', this.key).subscribe((result) => {
        trg.item = result;
      }, (error) => {
          // console.log(error);
      });
    }

    this.user = this.authProvider.getUser();
    if (this.key === '0') {
      this.item.company = this.user.userDetail.company;
    }
  }

  /*
  public getCompanies() {
    this.dataProvider.getFbList('companies').subscribe((companiesResult) => {
      this.companies = companiesResult;
    }, (error) => {
        // console.log(error);
    });
  }
  */

  public onSubmit() {
    this.dataProvider.addFbListItem('customers', this.item, this.key).subscribe((result) => {
      console.log('Result is: ', result);
      this.router.navigate(['../../customers']);
    });
  }
}
