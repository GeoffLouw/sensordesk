import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './customers.routes';
import { CustomersComponent } from './customers.component';
import { AppMaterialModule } from '../app.material.module';

@NgModule({
  declarations: [
    CustomersComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    AppMaterialModule,
    FlexLayoutModule
  ],
})

export class CustomersModule {
  public static routes = routes;
}
