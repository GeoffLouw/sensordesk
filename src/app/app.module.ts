import { NoteDialogComponent } from './_components/note-dialog.component';
import { TaskNotesModule } from './task-notes/task-notes.module';
import { StatusProvider } from './providers/status-provider';
import { TypeProvider } from './providers/types-provider';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule, ApplicationRef } from '@angular/core';
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';
import { AppMaterialModule } from './app.material.module';
/* Providers */
import { FlexLayoutModule } from '@angular/flex-layout';
import { RolesProvider } from './providers/roles-provider';
import { DataProvider } from './providers/data-provider';
import { LocationProvider } from './providers/location-provider';
import { AuthProvider } from './providers/auth-provider';
import { AccessProvider } from './providers/access-provider';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { ConfirmDialogService }   from './_components/confirm-dialog.service';
import { ConfirmDialogComponent }   from './_components/confirm-dialog.component';
import { AlertDialogsService }   from './_components/alert-dialog.service';
import { AlertDialogComponent }   from './_components/alert-dialog.component';
import { AppointmentsPopupComponent } from './appointments/appointment-popup.component';
import { WebStorageModule } from 'ngx-store';
import { DateTimePickerModule } from 'ngx-datetime-picker';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
/*import { AgmCoreModule } from 'angular2-google-maps/core';*/

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';
import { NoAccessComponent } from './no-access/no-access.component';
import { XLargeDirective } from './home/x-large';
import { DashboardComponent } from './dashboard';
import { LoginComponent } from './login';
import { SettingsComponent } from './settings';
import { RegisterUserComponent } from './register-user';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import '../styles/styles.scss';
import '../styles/headings.css';
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css';
import 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js';
import 'bootstrap-timepicker/css/bootstrap-timepicker.min.css';
import 'bootstrap-timepicker/js/bootstrap-timepicker.js';
import * as $ from 'jquery';

import 'fullcalendar';
import 'fullcalendar/dist/fullcalendar.min.css';
import { ScheduleModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
/*
import 'primeng/resources/themes/omega/theme.css';
import 'primeng/resources/primeng.min.css';
*/




// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};
/*
export const firebaseConfig = {
  apiKey: 'AIzaSyDEVXHN1-V10XBqkW3gNvMlhbLtrgu0PHM',
  authDomain: 'homeassistant-159601.firebaseapp.com',
  databaseURL: 'https://homeassistant-159601.firebaseio.com',
  projectId: 'homeassistant-159601',
  storageBucket: 'homeassistant-159601.appspot.com',
  messagingSenderId: '869958388852'
};
*/
export const firebaseConfig = {
  apiKey: 'AIzaSyCLBrNX35_UNrV-1eNTn6kqDZyXPsqkfgU',
  authDomain: 'geyser-install-db.firebaseapp.com',
  databaseURL: 'https://geyser-install-db.firebaseio.com',
  projectId: 'geyser-install-db',
  storageBucket: 'geyser-install-db.appspot.com',
  messagingSenderId: '711356452894'
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    LoginComponent,
    SettingsComponent,
    RegisterUserComponent,
    NoContentComponent,
    NoAccessComponent,
    XLargeDirective,
    ConfirmDialogComponent,
    AlertDialogComponent,
    AppointmentsPopupComponent,
    NoteDialogComponent
  ],
  entryComponents: [
    ConfirmDialogComponent,
    AlertDialogComponent,
    AppointmentsPopupComponent,
    NoteDialogComponent
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES, { useHash: true, preloadingStrategy: PreloadAllModules }),
    AngularFireModule.initializeApp(firebaseConfig),
    AppMaterialModule,
    FlexLayoutModule,
    WebStorageModule,
    DateTimePickerModule,
    NKDatetimeModule,
    ScheduleModule,
    DialogModule,
    CalendarModule,
    CheckboxModule,
    TaskNotesModule

    /*
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCCnR0DyOdxEXAju57jbAITVc9wccfm1do'
    })
    */
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    ENV_PROVIDERS,
    APP_PROVIDERS,
    DataProvider,
    LocationProvider,
    AuthProvider,
    RolesProvider,
    TypeProvider,
    StatusProvider,
    AccessProvider,
    AngularFireAuth,
    ConfirmDialogService,
    AlertDialogsService,
    AngularFireDatabase
  ]
})
export class AppModule {

  constructor(
    public appRef: ApplicationRef,
    public appState: AppState
  ) {}

  public hmrOnInit(store: StoreType) {
    if (!store || !store.state) {
      return;
    }
    console.log('HMR store', JSON.stringify(store, null, 2));
    // set state
    this.appState._state = store.state;
    // set input values
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }

    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  public hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
    // save state
    const state = this.appState._state;
    store.state = state;
    // recreate root elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // save input values
    store.restoreInputValues  = createInputTransfer();
    // remove styles
    removeNgStyles();
  }

  public hmrAfterDestroy(store: StoreType) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }

}
