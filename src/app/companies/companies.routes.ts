import { CompaniesComponent } from './companies.component';

export const routes = [
    { path: '', children: [
      { path: '', component: CompaniesComponent },
      { path: 'company-detail', loadChildren: './company-detail#CompanyDetailModule' }
    ]},
];
