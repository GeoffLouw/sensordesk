import { DataProvider } from './../providers/data-provider';
import { Component } from '@angular/core';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { ConfirmDialogComponent } from './../_components/confirm-dialog.component';

@Component({
  selector: 'page-companies',
  templateUrl: 'companies.component.html',
  styleUrls: ['./companies.component.scss']
})

export class CompaniesComponent {
  public items: any;
  constructor(
    private dataProvider: DataProvider,
    private dialog: MdDialog
    ) {
    this.init();
  }
  public init(){
    this.dataProvider.getCompanies().subscribe((result) => {
      console.log('Result: ', result);
      this.items = result;
    });
  }

  public deleteItem(item) {
    let dialogRef: MdDialogRef<ConfirmDialogComponent>;
    dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.componentInstance.title = 'Confirmation';
    dialogRef.componentInstance.message = 'Are you sure?';
    dialogRef.afterClosed().subscribe((confirmResponse) => {
      if (confirmResponse) {
        this.dataProvider.removeCompany(item).subscribe((removeResponse) => {
          console.log(removeResponse);
          this.init();
        });
      }
    });
  }
}
