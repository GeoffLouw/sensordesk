import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './company-detail.routes';
import { CompanyDetailComponent } from './company-detail.component';
import { AppMaterialModule } from '../../app.material.module';

@NgModule({
  declarations: [
    CompanyDetailComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    AppMaterialModule
  ],
  providers: []
})
export class CompanyDetailModule {
  public static routes = routes;
}
