import { CompanyDetailComponent } from './company-detail.component';

export const routes = [
  { path: '', component: CompanyDetailComponent,  pathMatch: 'full' },
];
