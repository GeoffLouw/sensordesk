import { Company1 } from './../../providers/api-client/model/Company1';
import { DataProvider } from '../../providers/data-provider';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'company-detail',
  templateUrl: 'company-detail.component.html',
  styleUrls: ['./company-detail.component.scss']
})
export class CompanyDetailComponent implements OnInit {
  public key: string = '';
  public item: Company1 = {
    name: '',
    detail: '',
    tel: '',
    email: ''
  };

  constructor(
    public db: AngularFireDatabase,
    private route: ActivatedRoute,
    private dataProvider: DataProvider,
    private router: Router
  ) {
    this.key = this.route.snapshot.queryParams['id'];
    let trg = this;
    if(this.key !== '0'){
      this.dataProvider.getCompany(this.key).subscribe((result) => {
        trg.item = result;
      }, (error) => {
          console.log(error);
      });
    }
  }
  public ngOnInit() {
    // console.log('hello `ChildDetail` component');
  }

  public onSubmit() {
    if(this.key === '0'){
      this.dataProvider.addCompany(this.item).subscribe((result) => {
        this.router.navigate(['../../companies']);
      });
    }else{
      this.dataProvider.updateCompany(this.item, this.key).subscribe((result) => {
        this.router.navigate(['../../companies']);
      });
    }
  }
}
