import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './companies.routes';
import { CompaniesComponent } from './companies.component';
import { AppMaterialModule } from '../app.material.module';

@NgModule({
  declarations: [
    CompaniesComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    AppMaterialModule
  ],
})
export class CompaniesModule {
  public static routes = routes;
}
