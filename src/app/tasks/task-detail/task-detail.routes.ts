import { TaskDetailComponent } from './task-detail.component';

export const routes = [
  { path: '', component: TaskDetailComponent,  pathMatch: 'full' },
];
