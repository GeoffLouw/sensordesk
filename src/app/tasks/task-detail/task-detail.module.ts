import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './task-detail.routes';
import { TaskDetailComponent } from './task-detail.component';
import { AppMaterialModule } from '../../app.material.module';
import { AgmCoreModule } from 'angular2-google-maps/core';

@NgModule({
  declarations: [
    TaskDetailComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    AppMaterialModule
  ],
  providers: []
})
export class TaskDetailModule {
  public static routes = routes;
}
