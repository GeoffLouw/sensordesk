import { TaskNotesModule } from './../task-notes/task-notes.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './tasks.routes';
import { TasksComponent } from './tasks.component';
import { AppMaterialModule } from '../app.material.module';
import { CalendarModule } from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';

@NgModule({
  declarations: [
    TasksComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    AppMaterialModule,
    FlexLayoutModule,
    CalendarModule,
    CheckboxModule,
    TaskNotesModule
  ],
})
export class TasksModule {
  public static routes = routes;
}
