import { TasksComponent } from './tasks.component';

export const routes = [
  { path: '', children: [
    { path: '', component: TasksComponent },
    { path: 'task-detail', loadChildren: './task-detail#TaskDetailModule' },
    { path: 'task', loadChildren: './task#TaskModule' }
  ]},
];
