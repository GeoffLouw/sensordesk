import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './task.routes';
import { TaskComponent } from './task.component';
import { AppMaterialModule } from '../../app.material.module';
import { AgmCoreModule } from 'angular2-google-maps/core';

@NgModule({
  declarations: [
    TaskComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    AppMaterialModule
  ],
  providers: []
})
export class TaskModule {
  public static routes = routes;
}
