import { Task1 } from './../../providers/api-client/model/Task1';
import { User1 } from './../../providers/api-client/model/User1';
import { LocationProvider } from './../../providers/location-provider';
import { DataProvider } from '../../providers/data-provider';
import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthProvider } from '../../providers/auth-provider';
import { Location } from '@angular/common'

interface userObject {
    ownerName: string,
    detail: string,
    company: string,
    email: string
    address: string,
    tel: string,
    status: string,
    type: string
}

declare let google;

@Component({
  selector: 'task-component',
  templateUrl: 'task.component.html',
  styleUrls: ['./task.component.scss']
})

export class TaskComponent implements OnInit {
  @ViewChild('addressFld') public addressFld: ElementRef;
  @ViewChild('smallmap') public mapElement: ElementRef;

  public key: string = '';
  /*
public item: userObject = {
    name: '',
    detail: '',
    company: '',
    email: '',
    tel:'',
    address:'',
    status: '',
    type: 'Appointment'
  }; */
  public item: Task1;

  public companies;
  public roles;
  public user;
  public address : Object;
  public placesOptions: Array<Object>;
  public map;
  public typeFilter;
  public addressFldVal: string;
  public addressMarker: any;

  constructor(
    private route: ActivatedRoute,
    private dataProvider: DataProvider,
    private authProvider: AuthProvider,
    private locationProvider: LocationProvider,
    private cd: ChangeDetectorRef,
    private router: Router,
    private location: Location
    ) {
    // this.items = db.list('/items');

    this.item = {
      userId: '',
      companyId: '',
      ownerName: '',
      status: Task1.StatusEnum.New,
      type: Task1.TypeEnum.Installation
    }
  }

  

  public ngOnInit() {
    this.key = this.route.snapshot.queryParams['id'];
    
    // console.log('hello `ChildDetail` component');
    
    if (this.key === '0') {
      this.getRoles();
      this.initTask();
    }else {
      this.dataProvider.getTask(this.key).subscribe((result) => {
        this.item = result;
        this.getRoles();
        this.initTask();
      }, (error) => {
          // console.log(error);
      });
    }
    this.user = this.authProvider.getUser();
    
  }
  public initTask(){
    let trg = this;
    if (this.key === '0') {
      /*this.item.company = this.user.userDetail.company;*/
      this.item.companyId = this.authProvider.getCompany();
    }

    this.loadMap();
    let map = this.map;
    let autocomplete = new google.maps.places.Autocomplete(this.addressFld.nativeElement);
    autocomplete.bindTo('bounds', this.map);
    this.clearAddressMarker();
    this.addressMarker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
      });
    autocomplete.addListener('place_changed', function() {
        let place = autocomplete.getPlace();
        trg.addPlace(place);
      });
    
    if(this.key !== '0'){
      this.placeMarker(this.item.address.geometry.location);
    }
  }
  public addPlace (place){
    this.item.address = place;//place['formatted_address'];
    this.addressFldVal = place['formatted_address'];
    if (!place.geometry) {
      // User entered the name of a Place that was not suggested and
      // pressed the Enter key, or the Place Details request failed.
      window.alert("No details available for input: '" + place.name + "'");
      return;
    }
    console.log(this.map);
    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      this.map.fitBounds(place.geometry.viewport);
    } else {
      this.map.setCenter(place.geometry.location);
      this.map.setZoom(17);  // Why 17? Because it looks good.
    }
    this.addressMarker.setPosition(place.geometry.location);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }
    this.cd.detectChanges()
  }
  public placeMarker (position) {
    this.clearAddressMarker();
    this.addressMarker = new google.maps.Marker({
      position: position,
      map: this.map
    });
    console.log(position);
    this.locationProvider.getLocationByLatLng(position).subscribe(
      (place) => {
        console.log(place);
        this.addPlace(place);
      });

    this.map.panTo(position);
  }
  public clearAddressMarker(){
    if(this.addressMarker){
      this.addressMarker.setMap(null);
      this.addressMarker = null;
    }
  }


  public loadMap () {
    let latLng = new google.maps.LatLng(-33.9224853,18.4072265);
    let trg = this;
    let mapOptions = {
      center: latLng,
      zoom: 15,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: true,
      rotateControl: true,
      fullscreenControl: true
    };
    
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let myloc = new google.maps.Marker({
        clickable: false,
        icon: new google.maps.MarkerImage('assets/img/mapicons/mobile_marker.png',
                                                        new google.maps.Size(22, 22),
                                                        new google.maps.Point(0, 0),
                                                        new google.maps.Point(11, 11),
                                                        new google.maps.Size(22, 22)
                                                        ),
        shadow: null,
        zIndex: 999,
        map: this.map
    });

    let circle = new google.maps.Circle({
      strokeColor: '#2f73c2',
      strokeOpacity: 0.0,
      strokeWeight: 0,
      map: this.map,
      radius: 100,    // 10 miles in metres
      fillColor: '#2f73c2'
    });

    circle.bindTo('center', myloc, 'position');
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      google.maps.event.trigger(this.map, 'resize');
    });
    google.maps.event.addListener(this.map, 'click', function(event) {
      trg.placeMarker(event.latLng);
    });

    this.locationProvider.getLocation().subscribe(
      (position) => {
        let mylocation = position;
        let me = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        myloc.setPosition(me);
        /* Dont extend bounds for current location
        bounds.extend(me);
        this.map.fitBounds(bounds);
        */
    });
  }
  public getAddress(place:Object) {       
      this.address = place['formatted_address'];
      var location = place['geometry']['location'];
      var lat =  location.lat();
      var lng = location.lng();
      console.log("Address Object", place);
  }

  public getRoles() {
    let trg = this;
    this.dataProvider.getCompanies().subscribe((companiesResult) => {
      this.companies = companiesResult;
    }, (error) => {
        // console.log(error);
    });
    this.dataProvider.getRoles().subscribe((rolesResult) => {
      this.roles = rolesResult;
      console.log('roles loaded', this.roles);
    });
  }

  public roleChange() {
    
  }
  public cancel () {
    this.location.back();
  }
  public onSubmit() {
    /*
    this.item.notes = [{
      timestamp: new Date().getTime(),
      note: "Cancellation note: Cancelled because of another test"
    }]
    */
    if(this.key !== '0'){
      this.dataProvider.updateTask(this.item, this.key).subscribe((result) => {
        console.log('Result is: ', result);
        this.router.navigate(['/appointments']);
        this.location.back();
      });
    }else{
      this.dataProvider.addTask(this.item).subscribe((result) => {
        console.log('Result is: ', result);
        this.router.navigate(['/appointments']);
      });
    }
  }
}
