import { TaskComponent } from './task.component';

export const routes = [
  { path: '', component: TaskComponent,  pathMatch: 'full' },
];
