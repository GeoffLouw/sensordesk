import { TaskDetailComponent } from './task-notes-detail.component';

export const routes = [
  { path: '', component: TaskDetailComponent,  pathMatch: 'full' },
];
