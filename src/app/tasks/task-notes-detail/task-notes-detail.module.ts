import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './task-notes-detail.routes';
import { TaskNotesDetailComponent } from './task-notes-detail.component';
import { AppMaterialModule } from '../../app.material.module';
import { AgmCoreModule } from 'angular2-google-maps/core';

@NgModule({
  declarations: [
    TaskNotesDetailComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    AppMaterialModule
  ],
  providers: []
})
export class TaskNotesDetailModule {
  public static routes = routes;
}
