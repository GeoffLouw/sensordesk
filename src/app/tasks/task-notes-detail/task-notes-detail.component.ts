import { ConfirmDialogComponent } from './../../_components/confirm-dialog.component';
import { MdDialogRef, MdDialog } from '@angular/material';
import { Task1 } from './../../providers/api-client/model/Task1';
import { StatusProvider } from './../../providers/status-provider';
import { TypeProvider } from './../../providers/types-provider';
import { DataProvider } from '../../providers/data-provider';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthProvider } from '../../providers/auth-provider';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { LocationProvider } from "../../providers/location-provider";
import { Location } from '@angular/common';

declare let google;

@Component({
  selector: 'task-detail',
  templateUrl: 'task-detail.component.html',
  styleUrls: ['./task-detail.component.scss']
})
export class TaskDetailComponent implements OnInit {
  @ViewChild('customermap') public mapElement: ElementRef;

  public key: string = '';
  public item: Task1 = {
    ownerName: '',
    detail: '',
    assignedDate: '',
    userId: '',
    companyId: '',
    type: Task1.TypeEnum.Installation,
    status: Task1.StatusEnum.Open
  };
  public types: any;
  public statuses: any;
  public companies: any;
  public agents: any;
  public agent: any;
  public customers: any;
  public customer: any;
  public user: any;
  public install: any;
  public lat: number = 51.678418;
  public lng: number = 7.809007;
  public map: any;
  public taskDate: Date;
  public icons: any = {
    blue: {
      icon: 'assets/img/mapicons/pin_blue.png'
    },
    red: {
      icon: 'assets/img/mapicons/pin_red.png'
    },
    yellow: {
      icon: 'assets/img/mapicons/pin_yellow.png'
    }
  };
  public bounds: any = new google.maps.LatLngBounds();

  constructor(
    private route: ActivatedRoute,
    private dataProvider: DataProvider,
    private authProvider: AuthProvider,
    private router: Router,
    private typeProvider: TypeProvider,
    private statusProvider: StatusProvider,
    public locationProvider: LocationProvider,
    private location: Location,
    private dialog: MdDialog
    ) {
  }

  public ngOnInit() {
    this.user = this.authProvider.getUser();
    this.key = this.route.snapshot.queryParams['id'];
    let trg = this;
    this.loadMap();
    
    if(this.key !== '0'){
      this.dataProvider.getTask(this.key).subscribe((result) => {
        trg.item = result;
        trg.taskDate = new Date(trg.item.assignedDate);
        trg.renderOnMap(trg.item);
        if (this.user.userDetail.role < 300) {
          this.getInstallData();
        }
        if(trg.item.userId){
          console.log("AGENT IS: ", trg.item.userId);
          this.dataProvider.getUser(trg.item.userId).subscribe((agentResult) => {
            console.log("AGENT:", agentResult);
            trg.agent = agentResult;
          });
        }
      });
    }
    
    this.types = this.typeProvider.getTypes();
    this.statuses = this.statusProvider.getStatuses();
    this.item.companyId = this.authProvider.getCompany();
    
  }

  public getInstallData() {
    this.install = this.item.install;
    console.log("INSTALL", this.item);
  }

  public getAgents() {
    this.dataProvider.getUsers().subscribe((usersResult) => {
        this.agents = usersResult.filter((hero) => {
        return (hero.role <= 400);
      });
    });
  }

  public loadMap () {
    let latLng = new google.maps.LatLng(-33.9224853,18.4072265);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: true,
      rotateControl: true,
      fullscreenControl: true
    };
    
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let myloc = new google.maps.Marker({
        clickable: false,
        icon: new google.maps.MarkerImage('assets/img/mapicons/mobile_marker.png',
                                                        new google.maps.Size(22, 22),
                                                        new google.maps.Point(0, 0),
                                                        new google.maps.Point(11, 11),
                                                        new google.maps.Size(22, 22)
                                                        ),
        shadow: null,
        zIndex: 999,
        map: this.map
    });

    let circle = new google.maps.Circle({
      strokeColor: '#2f73c2',
      strokeOpacity: 0.0,
      strokeWeight: 0,
      map: this.map,
      radius: 100,    // 10 miles in metres
      fillColor: '#2f73c2'
    });
    circle.bindTo('center', myloc, 'position');
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      google.maps.event.trigger(this.map, 'resize');
    });

    this.locationProvider.getLocation().subscribe(
      (position) => {
        let mylocation = position;
        let me = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        myloc.setPosition(me);
        
        this.bounds.extend(me);
        this.map.fitBounds(this.bounds);
    });
  }

  public getAddressLines(address){
    return address.split(',');
  }

  public renderOnMap(mapitem: any){
    let trg = this;
    this.locationProvider.getLocationByComponent(mapitem.address).subscribe((result) => {
        let position = result;
        let marker = new google.maps.Marker({
          map: this.map,
          position: position,
          icon: this.icons['blue'].icon, //defined by status (belongs to any appointments)
        });
        marker.setPosition(position);
        this.bounds.extend(position);
        this.map.fitBounds(this.bounds);
        google.maps.event.addListener(marker, 'click', () => {
          
        });
    })
  }
  public done() {
    this.location.back();
  }
  public editTask() {
    this.router.navigate(['/tasks/task'],{queryParams: {id:this.key}});
  }
  public cancelAppointment() {
    let dialogRef: MdDialogRef<ConfirmDialogComponent>;
    dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.componentInstance.title = 'Are you sure?';
    dialogRef.componentInstance.message = 'This task will be cancelled';
    dialogRef.afterClosed().subscribe((confirmResponse) => {
      if (confirmResponse) {
        this.item.assignedDate = "";
        this.item.status = Task1.StatusEnum.Cancelled;
        let note = 'Cancelled by ' + this.authProvider.userData.userDetail.userName + ' (' + this.authProvider.userData.userDetail.email  + ') ' ;
        this.dataProvider.addNote(this.item, note, ).subscribe(() => {
          this.dataProvider.updateTask(this.item, this.item.id).subscribe((removeResponse) => {
            console.log("canclled");
            this.location.back();
          });
        });
      }
    });
  }
  public rescheduleAppointment() {
    let dialogRef: MdDialogRef<ConfirmDialogComponent>;
    dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.componentInstance.title = 'Are you sure?';
    dialogRef.componentInstance.message = 'This will remove the tasks schedule.';
    dialogRef.afterClosed().subscribe((confirmResponse) => {
      if (confirmResponse) {
        this.item.assignedDate = "";
        this.item.status = Task1.StatusEnum.New;
        this.dataProvider.updateTask(this.item, this.item.id).subscribe((removeResponse) => {
          this.router.navigate(['/appointments/appointment-detail'],{queryParams: {id:this.item.id}});
        });
      }
    });
  }
  public onSubmit() {
    this.item.assignedDate = this.taskDate.toString();
    if(this.key !== '0'){
      this.dataProvider.updateTask(this.item, this.key).subscribe((result) => {
        console.log(this.router);
      });
    }else{
      this.dataProvider.addTask(this.item).subscribe((result) => {
        console.log(this.router);
      });
    }
  }
}
