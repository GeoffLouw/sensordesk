import { LocationProvider } from './../providers/location-provider';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthProvider } from './../providers/auth-provider';
import { DataProvider } from './../providers/data-provider';
import { Component, ViewChild, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';
import { MdDialogRef, MdDialog, MdDialogConfig, MdSidenav } from '@angular/material';
import { ConfirmDialogComponent } from './../_components/confirm-dialog.component';
import { User1 } from "../providers/api-client/index";

declare let google;
declare let MarkerClusterer;

@Component({
  selector: 'page-tasks',
  templateUrl: 'tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})

export class TasksComponent {
  @ViewChild('singlemap') public mapElement: ElementRef;
  @ViewChild('customerSidenav') public sideNavElement: MdSidenav;
  public items: any;
  public user: any;
  public map: any;
  public selTask: any;
  public selAgent: User1;
  public icons: any = {
    blue: {
      icon: 'assets/img/mapicons/pin_blue.png'
    },
    red: {
      icon: 'assets/img/mapicons/pin_red.png'
    },
    yellow: {
      icon: 'assets/img/mapicons/pin_yellow.png'
    },
    orange: {
      icon: 'assets/img/mapicons/pin_orange.png'
    }
  };
  public searchText: string;
  public markers: Array<any> = new Array();
  public itemsResult: any;
  public users: any;
  public markerCluster: any;
  public clusterStyles = [
    {
      textColor: 'white',
      url: 'assets/img/mapicons/m1.png',
      height: 53,
      width: 52
    },
    {
      textColor: 'white',
      url: 'assets/img/mapicons/m2.png',
      height: 56,
      width: 55
    },
    {
      textColor: 'white',
      url: 'assets/img/mapicons/m3.png',
      height: 66,
      width: 65
    }
  ];
  public clusteroptions = {
      imagePath: 'assets/img/mapicons/m',
      styles: this.clusterStyles
  };

  public filterObj = {
    name: '',
    area: '',
    agent: '',
    status: '',
    dates: {
      start: null,
      end: null
    }
  }
  public counter = {
    total: 0,
    count: 0
  }
  public areas: Array<string>;


  constructor(
      private dataProvider: DataProvider,
      private dialog: MdDialog,
      public authProvider: AuthProvider,
      public router: Router,
      private route: ActivatedRoute,
      public renderer: Renderer2,
      public locationProvider: LocationProvider,
      private cd: ChangeDetectorRef
    ) {
    
  }
  
  ngAfterViewInit() {
      this.init();
  }
  init() {
    // onsole.log('on after view init', this.mapElement);
      // this returns null
      this.user = this.authProvider.getUser();
      this.dataProvider.getTasks().subscribe((result) => {
        this.itemsResult = result;
        let statusFilter = (this.route.snapshot.queryParams['status']) ? (this.route.snapshot.queryParams['status']) : 'open';
        this.filterObj.status = statusFilter;
        //this.items = result.filter((item) => item.status === this.filterObj.status);
        this.filterTasks();
        this.getAreas('locality');
        //users
        this.dataProvider.getUsers().subscribe((usersResults) => {
          this.users = usersResults;
        });
        for (let i = 0; i < this.items.length; i++) {
          let itm = this.items[i];
          itm.status_color = '#cc0000';
        }
      });
      this.loadMap();
  }
  public getAreas (area) {
    let arr = this.locationProvider.getAddressComponents(this.items, area);
    this.areas = arr;
  }
  
  public renderOnMap(tasks: any){
    console.log("rendering on map", tasks);
    let bounds = new google.maps.LatLngBounds();
    let trg = this;
    this.markers = new Array();
    for(let i=0; i < tasks.length; i++){
      let task = tasks[i];
      let marker = new google.maps.Marker({
        map: this.map,
        icon: this.icons['orange'].icon, //defined by status (belongs to any appointments)
      });
      this.markers.push(marker);
      google.maps.event.addListener(marker, 'click', () => {
        this.selectTask(task);
      });
      this.locationProvider.getLocationByComponent(task.address).subscribe((result) => {
          let position = result;
          marker.setPosition(position);
          bounds.extend(position);
          this.map.fitBounds(bounds);
      });
    }


    //console.log(this.markers);

    this.markerCluster = new MarkerClusterer(this.map, this.markers, this.clusteroptions);
    this.markerCluster.setMaxZoom(16);
    this.markerCluster.redraw();
  }
  public filterByUser(event) {
    /*
    let userid = event.target.value;
    this.items = this.itemsResult.filter((item) => (item.status == this.filterObj.status && item.userId.indexOf(userid) >= 0));
    this.clearMap();
    this.renderOnMap(this.items);
    */
  }
  
  public clearDates() {
    this.filterObj.dates.start = null;
    this.filterObj.dates.end = null;
    this.filterTasks();
  }
  public filterTasks(){
    this.items = [];
    
    // filter on status
    this.items = this.itemsResult.filter((item) => (item.status == this.filterObj.status));
    this.counter.total = this.items.length;
    // filter by owner name
    this.items = this.items.filter((item) => (item.ownerName.toLowerCase().indexOf(this.filterObj.name.toLowerCase()) >= 0));
    // filter by agent
    this.items = this.items.filter((item) => (item.userId.indexOf(this.filterObj.agent) >= 0));
    // filter by area
    if(this.filterObj.area.length > 0){
      this.items = this.items.filter((item) => {
        let flag = false;
        let loc: any = this.locationProvider.getLocationValue(item, 'locality');
        if(loc.long_name == this.filterObj.area) flag = true;
        return flag;
      });
    }

    if(this.filterObj.dates.start && this.filterObj.dates.end){
      this.items = this.items.filter((item) => {
        let flag = false;
        let d1 = new Date(this.filterObj.dates.start);
        d1.setHours(0);
        d1.setMinutes(0);
        d1.setSeconds(0);
        let d2 = new Date(this.filterObj.dates.end);
        d2.setHours(23);
        d2.setMinutes(59);
        d2.setSeconds(59);
        let d3 = new Date(item.assignedDate);

        if(d3 > d1 && d3 < d2){
          flag = true;
        }
        return flag;
      });
    }
    
    this.counter.count = this.items.length;
    this.clearMap();
    this.renderOnMap(this.items);
  }

  public getAddressLines(address){
    return address.split(',');
  }
  public clearMap (){
    console.log(this.markers);
    for(var i=0; i < this.markers.length; i++){
      let marker = this.markers[i];
      marker.setMap(null);
      //marker = null;
    }
    this.markers = [];
    if(this.markerCluster)  this.markerCluster.setMap(null);
    console.log("Clustered", this.markerCluster);
  }

  public selectTask(customer){
    this.sideNavElement.open().then((result) => {
        console.log('why is this sometimes lagging?');
    });
    this.selTask = customer;
    this.selAgent = null;
    if(this.selTask.userId && this.selTask.userId.length > 0){
      this.dataProvider.getUser(this.selTask.userId).subscribe((userResult) => {
        this.selAgent = userResult;
      });
    }
    this.cd.detectChanges();
  }
  
  public loadMap () {
    let latLng = new google.maps.LatLng(-33.9224853,18.4072265);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: true,
      rotateControl: true,
      fullscreenControl: true
    };
    
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let myloc = new google.maps.Marker({
        clickable: false,
        icon: new google.maps.MarkerImage('assets/img/mapicons/mobile_marker.png',
                                                        new google.maps.Size(22, 22),
                                                        new google.maps.Point(0, 0),
                                                        new google.maps.Point(11, 11),
                                                        new google.maps.Size(22, 22)
                                                        ),
        shadow: null,
        zIndex: 999,
        map: this.map
    });

    let circle = new google.maps.Circle({
      strokeColor: '#2f73c2',
      strokeOpacity: 0.0,
      strokeWeight: 0,
      map: this.map,
      radius: 100,    // 10 miles in metres
      fillColor: '#2f73c2'
    });

    circle.bindTo('center', myloc, 'position');
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      google.maps.event.trigger(this.map, 'resize');
    });

    this.locationProvider.getLocation().subscribe(
      (position) => {
        let mylocation = position;
        let me = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        myloc.setPosition(me);
        /* Dont extend bounds for current location
        bounds.extend(me);
        this.map.fitBounds(bounds);
        */
    });
  }

  

  public viewTask (){
    this.router.navigate(['./tasks/task-detail'],{queryParams: {id:this.selTask.id}});
  }

  public deleteItem(item) {
    let dialogRef: MdDialogRef<ConfirmDialogComponent>;
    dialogRef = this.dialog.open(ConfirmDialogComponent);
    dialogRef.componentInstance.title = 'Confirmation';
    dialogRef.componentInstance.message = 'Are you sure?';
    dialogRef.afterClosed().subscribe((confirmResponse) => {
      this.clearMap();
      if (confirmResponse) {
        this.dataProvider.removeTask(item).subscribe((removeResponse) => {
          console.log(removeResponse);
          setTimeout(() => {
            this.init();
          },2000);
        });
      }
    });
  }
}
